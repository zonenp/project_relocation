class LinkValidator {
  
  linkLegal(obj: string): boolean {
    if(obj === '') {
      return false;
    }
    else if(obj) {
      const regex = new RegExp(
        '^(http|https):\\/\\/[(www.)]*[a-z0-9:]+.[a-z0-9][\\/a-zA-Z0-9?=&\\-%\\._]+$'
      );
      if(regex.test(obj)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}

export { LinkValidator }