import * as moment from 'moment';

class DateValidator {
  
  dateLegal(obj: Date): boolean {
    if(obj) {
      return true;
    }
    return false;
  }

  momentDateLegal(obj: moment.Moment) {
    if(obj) {
      return true;
    }
    return false;
  }
}

export { DateValidator }