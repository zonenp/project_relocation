import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortLinkText'
})
export class ShortLinkTextPipe implements PipeTransform {

  transform(link: string): string {
    const shortLink = link.substring(0, 40);
    return `${shortLink}...`;
  }

}
