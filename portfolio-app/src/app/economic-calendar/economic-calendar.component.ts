import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { CalendarEntry } from './src/calendar-entry/entry';
import { calendarEntryfromForexFactory } from './src/calendar-entry/factory';
import { CalendarEntryFollowupSource } from './src/followup-source/source.enum';
import { CalendarSource } from './src/calendar-source/source.enum';
import { evaluatedCalendarEntryFromForexFactory } from './src/evaluated-calendar-entry/factory';

// services
import { CalendarEntryFollowup } from './src/calendar-entry-followup/followup';
import { CalendarEntryFollowupService } from './entry-followup.service';
import { CalendarEntryService } from './entry.service';

@Component({
  selector: 'app-economic-calendar',
  templateUrl: './economic-calendar.component.html',
  styleUrls: ['./economic-calendar.component.scss']
})
export class EconomicCalendarComponent implements OnInit {

  endDate: moment.Moment = undefined;
  entries: Array<CalendarEntry>;
  followups: Array<CalendarEntryFollowup>;
  followupSource: CalendarEntryFollowupSource;
  followupSubject: string = undefined;
  maxEntries: Number = undefined;
  page: Number = undefined;
  pageFollowup: Number = undefined;
  pageSize: Number = undefined;
  pageSizeFollowup: Number = undefined;
  source: CalendarSource = undefined;
  startDate: moment.Moment = undefined;

  constructor(
    private _entryService: CalendarEntryService,
    private _followupService: CalendarEntryFollowupService
  ) { }

  clearFollowups(): void {
    this.followupSubject = undefined;
    this.followups = new Array<CalendarEntryFollowup>();
    this.pageFollowup = -1;
  }
  
  ngOnInit() {
    this.followupSource = CalendarEntryFollowupSource.DUCK_DUCK_GO;
    this.maxEntries = -1;
    this.page = -1;
    this.pageFollowup = -1;
    this.pageSize = 10;
    this.pageSizeFollowup = 10;
    this.source = CalendarSource.FOREX_FACTORY;
    this._setCalendarDatesRange();
    this.nextPage();
  }

  nextPage(): void {
    if(this.maxEntries < 1 || this._lastPageEntries()) {
      this.page = 0;
    } else {
      this.page = this.page.valueOf() + 1;
    }
    this._entries();
  }

  nextPageFollowup(entry: CalendarEntry): void {
    this.pageFollowup = this.pageFollowup.valueOf() + 1;
    this._followups(entry);
  }

  prevPage(): void {
    if(this.page.valueOf() > 0) {
      this.page = this.page.valueOf() - 1;
    }
    this._entries();
  }

  private _entries(): void {
    // TODO: test
    this._entryService.entries(this.source, this.page, this.pageSize, this.startDate, this.endDate)
    .subscribe((data) => {
      let result = new Array<CalendarEntry>();
      try {
        if(data['maxEntries'] == null || data['maxEntries'] == undefined) {
          throw new Error('Failed to receive max calendar entries value');
        } else if(data['entries'] == null || data['entries'] == undefined) {
          throw new Error('Failed to receive calendar entries value');
        }
        console.log(`Received\t${Object.keys(data['entries']).length}\tentries`);
        this.maxEntries = data['maxEntries'];
        for(let e in data['entries']) {
          const entryJson = data['entries'][e];
          let nextEntry = undefined;
          // previous index will not be present alone. It will be accompaniend with the next and actual.
          if(entryJson['previous']) {
            nextEntry = evaluatedCalendarEntryFromForexFactory(entryJson);
          } else {
            nextEntry = calendarEntryfromForexFactory(data['entries'][e]);
          }
          result.push(nextEntry);
        }
        this.entries = result;
      } catch(err) {
        console.log(`Failed to parse the raw calendar entries\n${err.message}`);
        return;
      }
    }, (err) => {
      this.entries = new Array<CalendarEntry>();
      console.error(`Failed to update the calendar entries\n${err.message}`);
    });
  }

  _followups(entry: CalendarEntry): void {
    try {
      this._followupService.updateEntry(entry);
      this.followupSubject = this._followupService.followupSubject();
    } catch(err) {
      console.error(`Failed to update the entry for the followup\n${err.message}`)
      return;
    }
    this._followupService.followup(this.followupSource, this.pageFollowup, this.pageSizeFollowup)
    .subscribe((data) => {
      let result = new Array<CalendarEntryFollowup>();
      try {
        for(let i in data) {
          let nextFollowup = new CalendarEntryFollowup(data[i]['title'], data[i]['link']);
          result.push(nextFollowup);
        } 
      } catch(err) {
        console.log(`Failed to parse the raw calendar entries followups\n${err.message}`);
        return;
      }
      this.followups = result;
    }, (err) => {
      this.clearFollowups();
      console.error(`Failed to update the calendar entry followup\n${err.message}`);
    })
  }

  private _lastPageEntries(): boolean {
    return this.page.valueOf()*this.pageSize.valueOf() >= this.maxEntries.valueOf();
  }

  private _setCalendarDatesRange(): void {
    this.startDate = moment().subtract(moment().day(), 'days');
    this.endDate = moment().subtract(moment().day(), 'days').add(4, 'days');
  }

}
