import { CalendarEntryFollowupValidator } from './validator';
import { Errors } from './errors.enum';

export class CalendarEntryFollowup {

  private _title: string = undefined;
  private _link: string = undefined;
  private _validator: CalendarEntryFollowupValidator = undefined;

  constructor(title: string, link: string) {
    this._validator = new CalendarEntryFollowupValidator();
    this._setTitle(title);
    this._setLink(link);
  }

  link(): string {
    return this._link;
  }

  private _setLink(obj: string): void {
    if(this.validator().linkLegal(obj)) {
      this._link = obj;
    } else {
      throw new Error(Errors.LINK_ILLEGAL);
    }
  }

  private _setTitle(obj: string): void {
    if(this.validator().titleLegal(obj)) {
      this._title = obj;
    } else {
      throw new Error(Errors.TITLE_ILLEGAL);
    }
  }

  private validator(): CalendarEntryFollowupValidator {
    return this._validator;
  }

  title(): string {
    return this._title;
  }
 }