import { StringValidator } from '../../../../assets/portfolio-app-js/validate/str';

export class CalendarEntryFollowupValidator {

  private _linkRegex: RegExp = undefined;
  private _stringValidator: StringValidator;
  
  constructor() {
    this._linkRegex = new RegExp(
      '^(http|https):\\/\\/[(www.)]*[a-z0-9:]+.[a-z0-9][\\/a-zA-Z0-9?!=&\\-%\\._]+$'
    );
    this._stringValidator = new StringValidator();
  }

  linkLegal(obj: string): boolean {
    if(obj === '') {
      return false;
    }
    else if(obj) {
      if(this.linkRegex().test(obj)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  linkRegex(): RegExp {
    return this._linkRegex;
  }

  titleLegal(obj: string): boolean {
    if(this._stringValidator.nonEmpty(obj)) {
      return true;
    } else {
      return false;
    }
  }
}