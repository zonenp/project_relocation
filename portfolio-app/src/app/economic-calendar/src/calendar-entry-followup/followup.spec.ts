import { CalendarEntryFollowup } from './followup';
import { Errors } from './errors.enum';

describe('CalendarEntryFollowup', () => {
  beforeAll(function() {
    this.legalTitle = 'demo-news-entry-followup';
    this.legalLinks = [
      'https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/may2019',
      'http://ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/may2019'
    ]
  });

  it('should create an instance with the legal arguments', function() {
    const a = new CalendarEntryFollowup(
      this.legalTitle,
      this.legalLinks[0]
    );
    expect(Object.getPrototypeOf(a).constructor.name).toEqual('CalendarEntryFollowup');
    const b = new CalendarEntryFollowup(
      this.legalTitle,
      this.legalLinks[1]
    );
    expect(Object.getPrototypeOf(b).constructor.name).toEqual('CalendarEntryFollowup');
  });

  describe('return of the legal object properties', function() {
    beforeAll(function() {
      this.followup = new CalendarEntryFollowup(
        this.legalTitle,
        this.legalLinks[0]
      );
    });

    it('should return a title', function() {
      expect(this.followup.title()).toEqual(this.legalTitle);
    });

    it('should return a link', function() {
      expect(this.followup.link()).toEqual(this.legalLinks[0]);
    });
  });

  describe('title argument', function() {
    it('should throw an error when the title is null', function() {
      expect(() => {
        new CalendarEntryFollowup(
          null,
          this.legalLinks[0]
        );
      }).toThrowError(Errors.TITLE_ILLEGAL)
    });
    module
    it('should throw an error when the title is undefined', function() {
      expect(() => {
        new CalendarEntryFollowup(
          undefined,
          this.legalLinks[0]
        );
      }).toThrowError(Errors.TITLE_ILLEGAL)
    });

    it('should throw an error when the title is empty', function() {
      expect(() => {
        new CalendarEntryFollowup(
          '',
          this.legalLinks[0]
        );
      }).toThrowError(Errors.TITLE_ILLEGAL)
    });
  });

  describe('link argument', function() {
    it('should throw an error when the link is null', function() {
      expect(() => {
        new CalendarEntryFollowup(
          this.legalTitle,
          null
        );
      }).toThrowError(Errors.LINK_ILLEGAL)
    });

    it('should throw an error when the title is undefined', function() {
      expect(() => {
        new CalendarEntryFollowup(
          this.legalTitle,
          undefined
        );
      }).toThrowError(Errors.LINK_ILLEGAL)
    });

    it('should throw an error when the link is empty', function() {
      expect(() => {
        new CalendarEntryFollowup(
          this.legalTitle,
          ''
        );
      }).toThrowError(Errors.LINK_ILLEGAL)
    });

    it('should throw an error when the link has an illegal structure', function() {
      expect(() => {
        new CalendarEntryFollowup(
          this.legalTitle,
          'this.com/news/here'
        );
      }).toThrowError(Errors.LINK_ILLEGAL)
    });
  });
});