export enum Errors {
  LINK_ILLEGAL = 'Link is illegal. Expected a __string__ matching the __RegExp__',
  TITLE_ILLEGAL = 'Title is illegal. Expected a non empty __string__'
}