import { CurrencyCode } from './currency-code.enum';

export class CurrencyCodeValidator {
  
  currencyLegal(obj: CurrencyCode): boolean {
    if(obj) {
      return true;
    } else {
      return false;
    }
  }

  currencyStrLegal(obj: string): boolean {
    if(obj && CurrencyCode[obj]) {
      return true;
    } else {
      return false;
    }
  }
}
