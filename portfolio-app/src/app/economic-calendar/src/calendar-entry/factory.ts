import { CalendarEntry } from './entry';
import { CurrencyCode } from '../currency-code/currency-code.enum';
import { currencyStrLegal, impactStrLegal } from './factory-validator';
import { Errors } from './errors.enum';
import { Impact } from '../impact/impact.enum';

function currencyFromString(obj: string) {
  if(currencyStrLegal(obj)) {
    return CurrencyCode[obj];
  } else {
    throw new Error(Errors.CURRENCY_STR_ILLEGAL);
  }
}

function impactFromString(obj: string) {
  if(impactStrLegal(obj)) {
    return Impact[obj.toLowerCase()];
  } else {
    throw new Error(Errors.IMPACT_STR_ILLEGAL);
  }
}

function calendarEntryfromForexFactory(obj: JSON) {
  const currency = currencyFromString(obj['country']);
  const date = new Date(obj['date']);
  const impact = impactFromString(obj['impact']);
  const title = obj['title'];
  return new CalendarEntry(title, currency, date, impact);
}

export { calendarEntryfromForexFactory }