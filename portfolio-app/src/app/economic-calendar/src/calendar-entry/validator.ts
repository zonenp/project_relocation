import { CurrencyCode } from '../currency-code/currency-code.enum';
import { CurrencyCodeValidator } from '../currency-code/validator';
import { DateValidator } from '../../../../assets/portfolio-app-js/validate/date';
import { Impact } from '../impact/impact.enum';
import { ImpactValidator } from '../impact/validator';
import { StringValidator } from '../../../../assets/portfolio-app-js/validate/str';

export class CalendarEntryValidator {
  
  private _currencyCodeValidator: CurrencyCodeValidator;
  private _dateValidator: DateValidator;
  private _impactValidator: ImpactValidator;
  private _stringValidator: StringValidator;

  constructor() {
    this._currencyCodeValidator = new CurrencyCodeValidator();
    this._dateValidator = new DateValidator();
    this._impactValidator = new ImpactValidator();
    this._stringValidator = new StringValidator();
  }

  currencyLegal(obj: CurrencyCode): boolean {
    return this._currencyCodeValidator.currencyLegal(obj);
  }

  impactLegal(obj: Impact): boolean {
    return this._impactValidator.impactLegal(obj);
  }

  dateLegal(obj: Date): boolean {
    return this._dateValidator.dateLegal(obj);
  }

  titleLegal(obj: string): boolean {
    if(this._stringValidator.nonEmpty(obj)) {
      return true;
    } else {
      return false;
    }
  }
}
