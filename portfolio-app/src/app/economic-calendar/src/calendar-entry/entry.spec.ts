import { CalendarEntry } from './entry';
import { CurrencyCode } from '../currency-code/currency-code.enum';
import { Errors } from './errors.enum';
import { Impact } from '../impact/impact.enum';

describe('CalendarEntry', () => {
  beforeAll(function() {
    this.legalTitle = 'demo-news-entry';
    this.legalCurrency = CurrencyCode.USD;
    this.legalDate = new Date('2019-05-19T19:01:00-04:00');
    this.legalImpact = Impact.high;
  });

  it('should create an instance with the legal arguments', function() {
    const a = new CalendarEntry(
      this.legalTitle,
      this.legalCurrency,
      this.legalDate,
      this.legalImpact
    );
    expect(Object.getPrototypeOf(a).constructor.name).toEqual('CalendarEntry');
  });

  it('should return true when compared to itself', function() {
    const a = new CalendarEntry(
      this.legalTitle,
      this.legalCurrency,
      this.legalDate,
      this.legalImpact
    );
    expect(a.compare(a)).toBeTruthy();
  });

  it('should return true when compared to an entry with the equal properties', function() {
    const a = new CalendarEntry(
      this.legalTitle,
      this.legalCurrency,
      this.legalDate,
      this.legalImpact
    );
    const b = new CalendarEntry(
      this.legalTitle,
      this.legalCurrency,
      this.legalDate,
      this.legalImpact
    );
    expect(a.compare(b)).toBeTruthy();
  });

  it('should return false when compared to an entry with the different properties', function() {
    const a = new CalendarEntry(
      this.legalTitle,
      this.legalCurrency,
      this.legalDate,
      this.legalImpact
    );
    const b = new CalendarEntry(
      this.legalTitle + '_not the same',
      this.legalCurrency,
      this.legalDate,
      this.legalImpact
    );
    expect(a.compare(b)).toBeFalsy();
  });

  describe('return of the legal object properties', function() {
    beforeAll(function() {
      this.entry = new CalendarEntry(
        this.legalTitle,
        this.legalCurrency,
        this.legalDate,
        this.legalImpact
      );
    });

    it('should return a title', function() {
      expect(this.entry.title()).toEqual(this.legalTitle);
    });

    it('should return a currency code', function() {
      expect(this.entry.currency()).toEqual(this.legalCurrency);
    });

    it('should return a date', function() {
      expect(this.entry.date()).toEqual(this.legalDate);
    });

    it('should return an impact', function() {
      expect(this.entry.impact()).toEqual(this.legalImpact);
    });
  });

  describe('title argument', function() {
    it('should throw an error when the title is null', function() {
      expect(() => {
        new CalendarEntry(
          null,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact
        );
      }).toThrowError(Errors.TITLE_ILLEGAL)
    });

    it('should throw an error when the title is undefined', function() {
      expect(() => {
        new CalendarEntry(
          undefined,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact
        );
      }).toThrowError(Errors.TITLE_ILLEGAL)
    });

    it('should throw an error when the title is empty', function() {
      expect(() => {
        new CalendarEntry(
          '',
          this.legalCurrency,
          this.legalDate,
          this.legalImpact
        );
      }).toThrowError(Errors.TITLE_ILLEGAL)
    });
  });

  describe('currency argument', function() {
    it('should throw an error when the currency is null', function() {
      expect(() => {
        new CalendarEntry(
          this.legalTitle,
          null,
          this.legalDate,
          this.legalImpact
        );
      }).toThrowError(Errors.CURRENCY_ILLEGAL);
    });

    it('should throw an error when the currency is undefined', function() {
      expect(() => {
        new CalendarEntry(
          this.legalTitle,
          undefined,
          this.legalDate,
          this.legalImpact
        );
      }).toThrowError(Errors.CURRENCY_ILLEGAL);
    });
  });

  describe('date argument', function() {
    it('should throw an error when the date is null', function() {
      expect(() => {
        new CalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          null,
          this.legalImpact
        );
      }).toThrowError(Errors.DATE_ILLEGAL);
    });

    it('should throw an error when the date is undefined', function() {
      expect(() => {
        new CalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          undefined,
          this.legalImpact
        );
      }).toThrowError(Errors.DATE_ILLEGAL);
    });
  });

  describe('impact argument', function() {
    it('should throw an error when the impact is null', function() {
      expect(() => {
        new CalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          null
        );
      }).toThrowError(Errors.IMPACT_ILLEGAL);
    });

    it('should throw an error when the impact is undefined', function() {
      expect(() => {
        new CalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          undefined
        );
      }).toThrowError(Errors.IMPACT_ILLEGAL);
    });
  });

});
