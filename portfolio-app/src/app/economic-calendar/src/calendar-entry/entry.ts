import { CalendarEntryValidator } from './validator';
import { CurrencyCode } from '../currency-code/currency-code.enum';
import { Errors } from './errors.enum';
import { Impact } from '../impact/impact.enum';

export class CalendarEntry {
  private _currency: CurrencyCode = undefined;
  private _date: Date = undefined;
  private _impact: Impact = undefined;
  private _title: string = undefined;
  protected _validator: CalendarEntryValidator = undefined;

  constructor(
    title: string,
    currency: CurrencyCode,
    date: Date,
    impact: Impact,
    validator = undefined
  ) {
    this._setValidator(validator);
    this._setCurrency(currency);
    this._setDate(date);
    this._setImpact(impact);
    this._setTitle(title);
  }

  compare(entry: CalendarEntry): boolean {
    if(entry) {
      if(this.title() === entry.title()) {
        if(this.currency() == entry.currency()) {
          return true;
        }
      }
    }
    return false;
  }
  
  currency(): CurrencyCode {
    return this._currency;
  }

  date(): Date {
    return this._date;
  }

  impact(): Impact {
    return this._impact;
  }

  title(): string {
    return this._title;
  }

  protected validator(): CalendarEntryValidator {
    return this._validator;
  }
  
  private _setCurrency(obj: CurrencyCode): void {
    if(this.validator().currencyLegal(obj)) {
      this._currency = obj;
    } else {
      throw new Error(Errors.CURRENCY_ILLEGAL);
    }
  }

  _setDate(obj: Date): void {
    if(this.validator().dateLegal(obj)) {
      this._date = obj;
    } else {
      throw new Error(Errors.DATE_ILLEGAL);
    }
  }
  
  _setImpact(obj: Impact): void {
    if(this.validator().impactLegal(obj)) {
      this._impact = obj;
    } else {
      throw new Error(Errors.IMPACT_ILLEGAL);
    }
  }

  _setTitle(obj: string): void {
    if(this.validator().titleLegal(obj)) {
      this._title = obj;
    } else {
      throw new Error(Errors.TITLE_ILLEGAL);
    }
  }

  private _setValidator(validator: CalendarEntryValidator): void {
    if(validator) {
      this._validator = validator;
    } else {
      this._validator = new CalendarEntryValidator();
    }
  }
}
