import { calendarEntryfromForexFactory } from './factory';
import { Errors } from './errors.enum';

describe('CalendarEntry Factory', () => {
  beforeAll(function() {
    this.legalTitle = 'demo-news-entry';
    this.legalStrCurrency = 'USD';
    this.legalStrDate = '2019-05-19T19:01:00-04:00';
    this.legalStrImpact = 'high'
  });

  it('should create an instance from the legal JSON from the ForexFactory', function() {
    let entryRawJson = `{"title":"${this.legalTitle}","country":"${this.legalStrCurrency}",`;
    entryRawJson += `"date":"${this.legalStrDate}","impact":"${this.legalStrImpact}"}`;
    const entryJson = JSON.parse(entryRawJson);
    const a = calendarEntryfromForexFactory(entryJson);
    expect(Object.getPrototypeOf(a).constructor.name).toEqual('CalendarEntry');
    expect(a.title()).toEqual(this.legalTitle);
    expect(a.currency()).toEqual(this.legalStrCurrency);
    expect(a.date()).toEqual(new Date(this.legalStrDate));
    expect(a.impact().toLowerCase()).toEqual(this.legalStrImpact);
  });

  describe('currency string argument', function() {
    beforeEach(function() {
      this.entryRawJson = `{"title":"${this.legalTitle}","impact":"${this.legalStrImpact}",`;
      this.entryRawJson += `"date":"${this.legalStrDate}"`;
    });

    it('should throw an error when the value is missing', function() {
      this.entryRawJson += '}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });
    
    it('should throw an error when the value is null', function() {
      this.entryRawJson += ',"country":null}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });

    it('should throw an error when the value is empty', function() {
      this.entryRawJson += ',"country":""}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });

    it('should throw an error when the value contains illegal value', function() {
      this.entryRawJson += ',"country":"some_COUntry"}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });
  });

  describe('impact string argument', function() {
    beforeEach(function() {
      this.entryRawJson = `{"title":"${this.legalTitle}","country":"${this.legalStrCurrency}",`;
      this.entryRawJson += `"date":"${this.legalStrDate}"`;
    });

    it('should throw an error when the value is missing', function() {
      this.entryRawJson += '}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });
    
    it('should throw an error when the value is null', function() {
      this.entryRawJson += ',"impact":null}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });

    it('should throw an error when the value is empty', function() {
      this.entryRawJson += ',"impact":""}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });

    it('should throw an error when the value contains illegal value', function() {
      this.entryRawJson += ',"impact":"Slightly medium"}'
      expect(() => {
        calendarEntryfromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });
  });
});