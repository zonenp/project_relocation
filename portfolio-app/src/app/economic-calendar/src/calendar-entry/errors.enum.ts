export enum Errors {
  CURRENCY_ILLEGAL = 'Currency is illegal. Expected a __CurrencyCode__',
  CURRENCY_STR_ILLEGAL = 'CurrencyCode string is illegal. Expected a __string__ that is a value in the CurrencyCode',
  DATE_ILLEGAL = 'Date is illegal. Expected a __Date__',
  IMPACT_ILLEGAL = 'Impact is illegal. Expected an __Impact__',
  IMPACT_STR_ILLEGAL = 'Impact string is illegal. Expected a __string__ that is a value in the Impact',
  TITLE_ILLEGAL = 'Title is illegal. Expected a non empty __string__'
}