export enum Errors {
  DATE_ILLEGAL = 'Date is illegal. Expected either __Date__ or __moment.Moment__',
  PAGE_ILLEGAL = 'Page is illegal. Expected a __Number__ >= 0',
  PAGE_SIZE_ILLEGAL = 'Page size is illegal. Expected a __Number__ > 0',
  SOURCE_ILLEGAL = 'Calendar source is illegal. Expected a __CalendarSource__'
}