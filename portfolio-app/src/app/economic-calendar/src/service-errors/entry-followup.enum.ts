export enum Errors {
  DATE_ILLEGAL = 'Date is illegal. Expected either __Date__ or __moment.Moment__',
  ENTRY_ILLEGAL = 'Entry is illegal. Expected a __CalendarEntry__',
  ENTRY_FOLLOWUP_SOURCE_ILLEGAL = 'Entry followup source is illegal. Expected a __CalendarEntryFollowupSource__',
  PAGE_ILLEGAL = 'Page is illegal. Expected a __Number__ >= 0',
  PAGE_SIZE_ILLEGAL = 'Page size is illegal. Expected a __Number__ > 0',
  TITLE_ILLEGAL = 'Title is illegal. Expected a non-empty __String__'
}