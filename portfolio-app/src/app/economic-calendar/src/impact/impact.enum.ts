export enum Impact {
  low = 'Low',
  medium = 'Medium',
  high = 'High',
  holiday = 'Holiday'
}
