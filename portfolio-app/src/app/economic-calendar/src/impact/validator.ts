import { Impact } from './impact.enum';

export class ImpactValidator {
  
  impactLegal(obj: Impact) {
    if(obj) {
      return true;
    } else {
      return false;
    }
  }

  impactStrLegal(obj: string) {
    if(obj && Impact[obj.toLowerCase()]) {
      return true;
    } else {
      return false;
    }
  }
}
