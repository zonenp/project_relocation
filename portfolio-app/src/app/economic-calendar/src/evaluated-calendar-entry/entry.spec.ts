import { CurrencyCode } from '../currency-code/currency-code.enum';
import { Errors as InheritedErrors } from '../calendar-entry/errors.enum';
import { Errors } from './errors.enum';
import { EvaluatedCalendarEntry } from './entry';
import { Impact } from '../impact/impact.enum';

describe('EvaluatedCalendarEntry', () => {
  beforeAll(function() {
    this.legalTitle = 'demo-news-entry';
    this.legalCurrency = CurrencyCode.USD;
    this.legalDate = new Date('2019-05-19T19:01:00-04:00');
    this.legalImpact = Impact.high;
    this.legalPrevious = '';
    this.legalActual = '10.0B';
    this.legalForecast = '-10.0B';
  });

  it('should create an instance with the legal arguments', function() {
    const a = new EvaluatedCalendarEntry(
      this.legalTitle,
      this.legalCurrency,
      this.legalDate,
      this.legalImpact,
      this.legalPrevious,
      this.legalForecast,
      this.legalActual
    );
    expect(Object.getPrototypeOf(a).constructor.name).toEqual('EvaluatedCalendarEntry');
  });

  describe('return of the legal object properties', function() {
    beforeAll(function() {
      this.entry = new EvaluatedCalendarEntry(
        this.legalTitle,
        this.legalCurrency,
        this.legalDate,
        this.legalImpact,
        this.legalPrevious,
        this.legalForecast,
        this.legalActual
      );
    });

    it('should return a title', function() {
      expect(this.entry.title()).toEqual(this.legalTitle);
    });

    it('should return a currency', function() {
      expect(this.entry.currency()).toEqual(this.legalCurrency);
    });

    it('should return a date', function() {
      expect(this.entry.date()).toEqual(this.legalDate);
    });

    it('should return an impact', function() {
      expect(this.entry.impact()).toEqual(this.legalImpact);
    });

    it('should return a previous value', function() {
      expect(this.entry.previous()).toEqual(this.legalPrevious);
    });

    it('should return a forecast value', function() {
      expect(this.entry.forecast()).toEqual(this.legalForecast);
    });

    it('should return an actual value', function() {
      expect(this.entry.actual()).toEqual(this.legalActual);
    });
  });

  describe('actual argument', function() {
    it('should throw an error when the value is null', function() {
      expect(() => {
        new EvaluatedCalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact,
          this.legalPrevious,
          this.legalForecast,
          null
        );
      }).toThrowError(Errors.ACTUAL_VALUE_ILLEGAL);
    });

    it('should throw an error when the value is undefined', function() {
      expect(() => {
        new EvaluatedCalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact,
          this.legalPrevious,
          this.legalForecast,
          null
        );
      }).toThrowError(Errors.ACTUAL_VALUE_ILLEGAL);;
    });
  });

  describe('forecast argument', function() {
    it('should throw an error when the value is null', function() {
      expect(() => {
        new EvaluatedCalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact,
          this.legalPrevious,
          null,
          this.legalActual
        );
      }).toThrowError(Errors.FORECAST_VALUE_ILLEGAL);
    });

    it('should throw an error when the value is undefined', function() {
      expect(() => {
        new EvaluatedCalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact,
          this.legalPrevious,
          undefined,
          this.legalActual
        );
      }).toThrowError(Errors.FORECAST_VALUE_ILLEGAL);
    });
  });

  describe('previous argument', function() {
    it('should throw an error when the value is null', function() {
      expect(() => {
        new EvaluatedCalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact,
          null,
          this.legalForecast,
          this.legalActual
        );
      }).toThrowError(Errors.PREVIOUS_VALUE_ILLEGAL);
    });

    it('should throw an error when the value is undefined', function() {
      expect(() => {
        new EvaluatedCalendarEntry(
          this.legalTitle,
          this.legalCurrency,
          this.legalDate,
          this.legalImpact,
          undefined,
          this.legalForecast,
          this.legalActual
        );
      }).toThrowError(Errors.PREVIOUS_VALUE_ILLEGAL);
    });
  });

  it('should use an appropriate validator for the inherited properties', function() {
    expect(() => {
      new EvaluatedCalendarEntry(
        undefined,
        this.legalCurrency,
        this.legalDate,
        this.legalImpact,
        this.legalPrevious,
        this.legalForecast,
        this.legalActual
      );
    }).toThrowError(InheritedErrors.TITLE_ILLEGAL);
  });
});
