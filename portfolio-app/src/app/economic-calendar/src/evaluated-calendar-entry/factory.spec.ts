import { CurrencyCode } from '../currency-code/currency-code.enum';
import { Impact } from '../impact/impact.enum';
import { evaluatedCalendarEntryFromForexFactory } from './factory';
import { Errors } from './errors.enum';

describe('EvaluatedCalendarEntry', () => {
  beforeAll(function() {
    this.legalTitle = 'demo-news-entry';
    this.legalCurrency = CurrencyCode.USD;
    this.legalStrCurrency = 'USD';
    this.legalDate = new Date('2019-05-19T19:01:00-04:00');
    this.legalStrDate = '2019-05-19T19:01:00-04:00';
    this.legalImpact = Impact.high;
    this.legalStrImpact = 'high';
    this.legalPrevious = '';
    this.legalActual = '10.0B';
    this.legalForecast = '-10.0B';
  });

  it('should create an instance from the legal JSON with no valued fields, i.e. actual', function() {
    let entryRawJson = `{"title":"${this.legalTitle}","country":"${this.legalStrCurrency}",`;
    entryRawJson += `"date":"${this.legalStrDate}","impact":"${this.legalStrImpact}"}`;
    const entryJson = JSON.parse(entryRawJson);
    const a = evaluatedCalendarEntryFromForexFactory(entryJson);
    expect(Object.getPrototypeOf(a).constructor.name).toEqual('EvaluatedCalendarEntry');
    expect(a.title()).toEqual(this.legalTitle);
    expect(a.currency()).toEqual(this.legalCurrency);
    expect(a.date()).toEqual(this.legalDate);
    expect(a.impact()).toEqual(this.legalImpact);
    expect(a.previous()).toEqual('');
    expect(a.forecast()).toEqual('');
    expect(a.actual()).toEqual('');
  });

  it('should create an instance from the legal JSON with valued fields, i.e. previous', function() {
    let entryRawJson = `{"title":"${this.legalTitle}","country":"${this.legalStrCurrency}",`;
    entryRawJson += `"date":"${this.legalStrDate}","impact":"${this.legalStrImpact}",`;
    entryRawJson += `"previous":"${this.legalPrevious}","forecast":"${this.legalForecast}",`;
    entryRawJson += `"actual":"${this.legalActual}"}`;
    const entryJson = JSON.parse(entryRawJson);
    const a = evaluatedCalendarEntryFromForexFactory(entryJson);
    expect(Object.getPrototypeOf(a).constructor.name).toEqual('EvaluatedCalendarEntry');
    expect(a.title()).toEqual(this.legalTitle);
    expect(a.currency()).toEqual(this.legalCurrency);
    expect(a.date()).toEqual(this.legalDate);
    expect(a.impact()).toEqual(this.legalImpact);
    expect(a.previous()).toEqual(this.legalPrevious);
    expect(a.forecast()).toEqual(this.legalForecast);
    expect(a.actual()).toEqual(this.legalActual);
  });

  describe('currency string argument',function() {
    beforeEach(function() {
      this.entryRawJson = `{"title":"${this.legalTitle}",`;
      this.entryRawJson += `"date":"${this.legalStrDate}","impact":"${this.legalStrImpact}"`;
    });

    it('should throw an error when the value is missing', function() {
      this.entryRawJson += '}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });

    it('should throw an error when the value is null', function() {
      this.entryRawJson += ',"country":null}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });

    it('should throw an error when the value is empty', function() {
      this.entryRawJson += ',"country":""}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });

    it('should throw an error when the value is wrong', function() {
      this.entryRawJson += ',"country":"SomeCountry"}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.CURRENCY_STR_ILLEGAL);
    });
  });

  describe('impact string argument',function() {
    beforeEach(function() {
      this.entryRawJson = `{"title":"${this.legalTitle}","country":"${this.legalStrCurrency}",`;
      this.entryRawJson += `"date":"${this.legalStrDate}"`;
    });

    it('should throw an error when the value is missing', function() {
      this.entryRawJson += '}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });

    it('should throw an error when the value is null', function() {
      this.entryRawJson += ',"impact":null}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });

    it('should throw an error when the value is empty', function() {
      this.entryRawJson += ',"impact":""}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });

    it('should throw an error when the value is wrong', function() {
      this.entryRawJson += ',"impact":"Medium Well"}';
      expect(() => {
        evaluatedCalendarEntryFromForexFactory(JSON.parse(this.entryRawJson));
      }).toThrowError(Errors.IMPACT_STR_ILLEGAL);
    });
  });
});
