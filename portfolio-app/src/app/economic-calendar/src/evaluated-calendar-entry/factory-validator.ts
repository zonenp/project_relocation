import { CurrencyCodeValidator } from '../currency-code/validator';
import { ImpactValidator } from '../impact/validator';

function currencyStrLegal(obj: string): boolean {
  return CurrencyCodeValidator.prototype.currencyStrLegal(obj);
}

function impactStrLegal(obj: string): boolean {
  return ImpactValidator.prototype.impactStrLegal(obj);
}

export { currencyStrLegal, impactStrLegal }