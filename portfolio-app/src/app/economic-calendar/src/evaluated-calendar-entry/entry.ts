import { CurrencyCode } from '../currency-code/currency-code.enum';
import { CalendarEntry } from '../calendar-entry/entry';
import { Errors } from './errors.enum';
import { EvaluatedCalendarEntryValidator } from './validator';
import { Impact } from '../impact/impact.enum';

export class EvaluatedCalendarEntry extends CalendarEntry {
  private _previous: string;
  private _forecast: string;
  private _actual: string;

  constructor(
    title: string,
    currency: CurrencyCode,
    date: Date,
    impact: Impact,
    previous: string,
    forecast: string,
    actual: string
  ) {
    super(title, currency, date, impact, new EvaluatedCalendarEntryValidator());
    this._setPrevious(previous);
    this._setForecast(forecast);
    this._setActual(actual);
  }

  actual(): string {
    return this._actual;
  }
  
  forecast(): string {
    return this._forecast;
  }
  
  previous(): string {
    return this._previous;
  }

  protected validator(): EvaluatedCalendarEntryValidator {
    return this._validator as EvaluatedCalendarEntryValidator;
  }

  _setActual(obj: string): void {
    if(this.validator().valueActualLegal(obj)) {
      this._actual = obj;
    } else {
      throw new Error(Errors.ACTUAL_VALUE_ILLEGAL);
    }
  }
  
  _setForecast(obj: string): void {
    if(this.validator().valueForecastLegal(obj)) {
      this._forecast = obj;
    } else {
      throw new Error(Errors.FORECAST_VALUE_ILLEGAL);
    }
  }
  
  _setPrevious(obj: string): void {
    if(this.validator().valuePreviousLegal(obj)) {
      this._previous = obj;
    } else {
      throw new Error(Errors.PREVIOUS_VALUE_ILLEGAL);
    }
  }
  
}
