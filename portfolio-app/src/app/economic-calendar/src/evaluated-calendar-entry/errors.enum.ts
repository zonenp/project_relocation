export enum Errors {
  ACTUAL_VALUE_ILLEGAL = 'Actual value is illegal. Expected a __string__',
  CURRENCY_STR_ILLEGAL = 'CurrencyCode string is illegal. Expected a __string__ that is a value in the CurrencyCode',
  IMPACT_STR_ILLEGAL = 'Impact string is illegal. Expected a __string__ that is a value in the Impact',
  FORECAST_VALUE_ILLEGAL = 'Forecast value is illegal. Expected a __string__',
  PREVIOUS_VALUE_ILLEGAL = 'Previous value is illegal. Expected a __string__'
}