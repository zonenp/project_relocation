import { CalendarEntryValidator } from '../calendar-entry/validator';

class EvaluatedCalendarEntryValidator extends CalendarEntryValidator {
  
  valueActualLegal(obj: string): boolean {
    if(obj === '') {
      return true;
    } else if(obj) {
      return true;
    } {
      return false;
    }
  }

  valueForecastLegal(obj: string): boolean {
    if(obj === '') {
      return true;
    } else if(obj) {
      return true;
    } else {
      return false;
    }
  }

  valuePreviousLegal(obj: string): boolean {
    if(obj === '') {
      return true;
    } else if(obj) {
      return true;
    } else {
      return false;
    }
  }
}

export { EvaluatedCalendarEntryValidator }