import { CurrencyCode } from '../currency-code/currency-code.enum';
import { impactStrLegal, currencyStrLegal } from './factory-validator';
import { Impact } from '../impact/impact.enum';
import { Errors } from './errors.enum';
import { EvaluatedCalendarEntry } from './entry';

function currencyFromString(obj: string) {
  if(currencyStrLegal(obj)) {
    return CurrencyCode[obj];
  } else {
    throw new Error(Errors.CURRENCY_STR_ILLEGAL);
  }
}

function impactFromString(obj: string) {
  if(impactStrLegal(obj)) {
    return Impact[obj.toLowerCase()];
  } else {
    throw new Error(Errors.IMPACT_STR_ILLEGAL);
  }
}

function evaluatedCalendarEntryFromForexFactory(obj: JSON): EvaluatedCalendarEntry {
  const title = obj['title'];
  const currency = currencyFromString(obj['country']);
  const date = new Date(obj['date']);
  const impact = impactFromString(obj['impact']);
  const previous = obj['previous'] ? obj['previous'] : '';
  const forecast = obj['forecast'] ? obj['forecast'] : '';
  const actual = obj['actual'] ? obj['actual'] : '';
  return new EvaluatedCalendarEntry(title, currency, date, impact, previous, forecast, actual);
}

export { evaluatedCalendarEntryFromForexFactory }