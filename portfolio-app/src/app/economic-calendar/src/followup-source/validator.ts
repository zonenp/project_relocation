import { Injectable } from '@angular/core';
import { CalendarEntryFollowupSource } from './source.enum';

@Injectable({
  providedIn: 'root'
})
export class CalendarEntryFollowupSourceValidator {
  sourceLegal(source: CalendarEntryFollowupSource) {
    if(source) {
      return true;
    } else {
      return false;
    }
  }

  sourceStrLegal(obj: string) {
    if(obj && CalendarEntryFollowupSource[obj]) {
      return true;
    } else {
      return false;
    }
  }
}
