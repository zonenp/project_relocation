import { CalendarEntry } from '../calendar-entry/entry';
import { CurrencyCode } from '../currency-code/currency-code.enum';
import { EvaluatedCalendarEntry } from '../evaluated-calendar-entry/entry';
import { Impact } from '../impact/impact.enum';

const ENTRIES: CalendarEntry[] = [
  new CalendarEntry('European Parliamentary Elections', CurrencyCode.EUR, new Date('2019-05-26T06:16:00-04:00'),
  Impact.high),
  new CalendarEntry('BOJ Gov Kuroda Speaks', CurrencyCode.JPY, new Date('2019-05-26T23:00:00-04:00'), Impact.medium),
  new CalendarEntry('Bank Holiday', CurrencyCode.GBP, new Date('2019-05-27T03:00:00-04:00'), Impact.holiday),
  new CalendarEntry('Bank Holiday', CurrencyCode.USD, new Date('2019-05-27T08:01:00-04:00'), Impact.holiday),
  new CalendarEntry('RBNZ Financial Stability Report', CurrencyCode.NZD, new Date('2019-05-28T17:00:00-04:00'),
  Impact.high),
  new EvaluatedCalendarEntry('SPPI y\/y', CurrencyCode.JPY, new Date('2019-05-27T19:50:00-04:00'), Impact.low,
  '1.1%', '1.2%', ''),
  new EvaluatedCalendarEntry('BOJ Core CPI y\/y', CurrencyCode.JPY, new Date('2019-05-27T19:50:00-04:00'), Impact.low,
  '0.5%', '', ''),
  new EvaluatedCalendarEntry('GDP q\/q', CurrencyCode.CHF, new Date('2019-05-27T19:50:00-04:00'), Impact.low,
  '0.2%', '0.3%', ''),
  new EvaluatedCalendarEntry('German GfK Consumer Climate', CurrencyCode.EUR, new Date('2019-05-27T19:50:00-04:00'),
  Impact.low, '10.4', '10.4', ''),
  new EvaluatedCalendarEntry('Trade Balance', CurrencyCode.CHF, new Date('2019-05-27T19:50:00-04:00'),
  Impact.low, '2.98B', '3.18B', '3.17B')
];

const ENTRIES_RAW = '{"0":{"title":"Bank Holiday","country":"NZD","date":"2019-10-27T16:00:00-04:00","impact":"Holiday","forecast":"","previous":""},"1":{"title":"SPPI y/y","country":"JPY","date":"2019-10-27T19:50:00-04:00","impact":"Low","forecast":"0.5%","previous":"0.6%"},"2":{"title":"German Import Prices m/m","country":"EUR","date":"2019-10-28T03:00:00-04:00","impact":"Low","forecast":"0.1%","previous":"-0.6%"},"3":{"title":"M3 Money Supply y/y","country":"EUR","date":"2019-10-28T05:00:00-04:00","impact":"Low","forecast":"5.7%","previous":"5.7%"},"4":{"title":"Private Loans y/y","country":"EUR","date":"2019-10-28T05:00:00-04:00","impact":"Low","forecast":"3.4%","previous":"3.4%"},"5":{"title":"CBI Realized Sales","country":"GBP","date":"2019-10-28T07:00:00-04:00","impact":"Low","forecast":"-20","previous":"-16"},"6":{"title":"Goods Trade Balance","country":"USD","date":"2019-10-28T08:30:00-04:00","impact":"Low","forecast":"-73.5B","previous":"-72.8B"},"7":{"title":"Prelim Wholesale Inventories m/m","country":"USD","date":"2019-10-28T08:30:00-04:00","impact":"Low","forecast":"0.3%","previous":"0.4%"},"8":{"title":"ECB President Draghi Speaks","country":"EUR","date":"2019-10-28T11:00:00-04:00","impact":"High","forecast":"","previous":""},"9":{"title":"MPC Member Tenreyro Speaks","country":"GBP","date":"2019-10-28T13:00:00-04:00","impact":"Low","forecast":"","previous":""}}'

export { ENTRIES, ENTRIES_RAW}