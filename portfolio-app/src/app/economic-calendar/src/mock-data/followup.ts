import { CalendarEntry } from '../calendar-entry/entry';
import { CurrencyCode } from '../currency-code/currency-code.enum';
import { Impact } from '../impact/impact.enum';

const ENTRY = new CalendarEntry(
    'Public Sector Net Borrowing',
    CurrencyCode.EUR,
    new Date('2019-05-26T06:16:00-04:00'),
    Impact.high
);
const FOLLOWUPS = {
  "0": {
    "link": "https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance",
    "title": "Public  sector finance - Office for National Statistics"
  },
  "1": {
      "link": "https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/august2019",
      "title": "Public  sector finances, UK - Office for National Statistics"
  },
  "2": {
      "link": "https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/may2019",
      "title": "Public  sector finances, UK - Office for National Statistics"
  },
  "3": {
      "link": "https://www.ons.gov.uk/releases/publicsectorfinancesukaugust2019",
      "title": "Public  sector finances, UK: August 2019 - Office for National ..."
  },
  "4": {
      "link": "https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/june2019",
      "title": "Public  sector finances, UK - Office for National Statistics"
  },
  "5": {
      "link": "https://cy.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/may2019/relateddata",
      "title": "All data related to Public  sector finances, UK: May 2019 ..."
  },
  "6": {
      "link": "https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/january2019",
      "title": "Public  sector finances, UK - Office for National Statistics"
  },
  "7": {
      "link": "https://www.investing.com/economic-calendar/public-sector-net-borrowing-253",
      "title": "United Kingdom Public  Sector  Net  Borrowing - Investing.com"
  },
  "8": {
      "link": "https://obr.uk/fiscal_categories/public-sector-net-borrowing/",
      "title": "Public  sector  net  borrowing - Office for Budget Responsibility"
  },
  "9": {
      "link": "https://tradingeconomics.com/united-kingdom/government-debt",
      "title": "United Kingdom Public  Sector  Net  Borrowing | 2019 | Data ..."
  }
}

const FOLLOWUPS_RAW = '{"0":{"link":"https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/may2019","title":"Public  sector finances, UK - Office for National Statistics"},"1":{"link":"https://tradingeconomics.com/united-kingdom/government-debt","title":"United Kingdom Public  Sector  Net  Borrowing | 2019 | Data ..."},"2":{"link":"https://www.investing.com/economic-calendar/public-sector-net-borrowing-253","title":"United Kingdom Public  Sector  Net  Borrowing - Investing.com"},"3":{"link":"https://www.mql5.com/en/economic-calendar/united-kingdom/public-sector-net-borrowing","title":"Public  Sector  Net  Borrowing - economic indicator from the ..."},"4":{"link":"https://www.nasdaq.com/article/uk-public-sector-net-borrowing-july-gbp-15bln-vs-22bln-exp-cm667507","title":"UK public  sector  net  borrowing July GBP -1.5bln vs -2.2bln ..."},"5":{"link":"https://obr.uk/fiscal_categories/public-sector-net-borrowing/","title":"Public  sector  net  borrowing - Office for Budget Responsibility"},"6":{"link":"https://www.forexfactory.com/news/926544-public-sector-finances-uk-may-2019","title":"Public  sector finances, UK: May 2019 @ Forex Factory"},"7":{"link":"https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/842880/PSF_bulletin_September_2019_corrected_HMT_V2.pdf","title":"PDF  Public  sector finances, UK: September 2019"},"8":{"link":"https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance/bulletins/publicsectorfinances/january2019","title":"Public  sector finances, UK - Office for National Statistics"},"9":{"link":"https://www.ons.gov.uk/economy/governmentpublicsectorandtaxes/publicsectorfinance","title":"Public  sector finance - Office for National Statistics"}}'

export { ENTRY, FOLLOWUPS, FOLLOWUPS_RAW };