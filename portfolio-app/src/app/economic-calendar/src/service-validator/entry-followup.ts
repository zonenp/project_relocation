import { CalendarEntry } from '../calendar-entry/entry';
import { CalendarEntryFollowupSource } from '../followup-source/source.enum';
import { CalendarEntryFollowupSourceValidator } from '../followup-source/validator';
import { DateValidator } from '../../../../assets/portfolio-app-js/validate/date';
import { PageValidator } from '../../../../assets/portfolio-app-js/validate/page';
import { PageSizeValidator } from '../../../../assets/portfolio-app-js/validate/page-size';
import { StringValidator } from '../../../../assets/portfolio-app-js/validate/str';

export class CalendarEntryFollowupServiceValidator {

  private _dateValidator: DateValidator;
  private _pageValidator: PageValidator;
  private _pageSizeValidator: PageSizeValidator;
  private _sourceValidator: CalendarEntryFollowupSourceValidator;
  private _stringValidator: StringValidator

  constructor() {
    this._dateValidator = new DateValidator();
    this._pageValidator = new PageValidator();
    this._pageSizeValidator = new PageSizeValidator();
    this._sourceValidator = new CalendarEntryFollowupSourceValidator();
    this._stringValidator = new StringValidator();
  }

  dateLegal(obj): boolean {
    if(Object.getPrototypeOf(obj).constructor.name === 'Date') {
      return this.dateValidator().dateLegal(obj);
    } else {
      return this.dateValidator().momentDateLegal(obj);
    }
  }

  entryLegal(obj: CalendarEntry) {
    if(obj && obj.title() != '' && obj.date()) {
      return true;
    } else {
      return false;
    }
  }

  followupSourceLegal(obj: CalendarEntryFollowupSource): boolean {
    return this.sourceValidator().sourceLegal(obj);
  }

  pageLegal(obj: Number): boolean {
    return this.pageValidator().pageLegal(obj);
  }

  pageSizeLegal(obj: Number): boolean {
    return this.pageSizeValidator().pageSizeLegal(obj);
  }

  titleLegal(obj: String): boolean {
    try {
      return this.stringValidator().nonEmpty(obj.toString());
    } catch(err) {
      return false;
    }
  }

  private dateValidator(): DateValidator {
    return this._dateValidator;
  }

  private pageValidator(): PageValidator {
    return this._pageValidator;
  }

  private pageSizeValidator(): PageSizeValidator {
    return this._pageSizeValidator;
  }

  private sourceValidator(): CalendarEntryFollowupSourceValidator {
    return this._sourceValidator;
  }

  private stringValidator(): StringValidator {
    return this._stringValidator;
  }
  
}