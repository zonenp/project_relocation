import { CalendarSource } from '../calendar-source/source.enum';
import { CalendarSourceValidator } from '../calendar-source/validator';
import { DateValidator } from '../../../../assets/portfolio-app-js/validate/date';
import { PageValidator } from '../../../../assets/portfolio-app-js/validate/page';
import { PageSizeValidator } from '../../../../assets/portfolio-app-js/validate/page-size';

export class CalendarEntryServiceValidator {

  private _dateValidator: DateValidator;
  private _pageValidator: PageValidator;
  private _pageSizeValidator: PageSizeValidator;
  private _sourceValidator: CalendarSourceValidator;

  constructor() {
    this._dateValidator = new DateValidator();
    this._pageValidator = new PageValidator();
    this._pageSizeValidator = new PageSizeValidator();
    this._sourceValidator = new CalendarSourceValidator();
  }

  calendarSourceLegal(obj: CalendarSource): boolean {
    return this.sourceValidator().calendarSourceLegal(obj);
  }

  dateLegal(obj): boolean {
    if(Object.getPrototypeOf(obj).constructor.name === 'Date') {
      return this.dateValidator().dateLegal(obj);
    } else {
      return this.dateValidator().momentDateLegal(obj);
    }
  }

  pageLegal(obj: Number): boolean {
    return this.pageValidator().pageLegal(obj);
  }

  pageSizeLegal(obj: Number): boolean {
    return this.pageSizeValidator().pageSizeLegal(obj);
  }

  private dateValidator(): DateValidator {
    return this._dateValidator;
  }

  private pageValidator(): PageValidator {
    return this._pageValidator;
  }

  private pageSizeValidator(): PageSizeValidator {
    return this._pageSizeValidator;
  }

  private sourceValidator(): CalendarSourceValidator {
    return this._sourceValidator;
  }
}