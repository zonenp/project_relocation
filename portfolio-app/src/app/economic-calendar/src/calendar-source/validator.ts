import { Injectable } from '@angular/core';
import { CalendarSource } from './source.enum';

@Injectable({
  providedIn: 'root'
})
export class CalendarSourceValidator {
  calendarSourceLegal(source: CalendarSource) {
    if(source) {
      return true;
    } else {
      return false;
    }
  }

  calendarSourceStrLegal(obj: string) {
    if(obj && CalendarSource[obj]) {
      return true;
    } else {
      return false;
    }
  }
}
