import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';

import { CalendarEntryFollowupService } from './entry-followup.service';
import { CalendarEntryFollowupSource } from './src/followup-source/source.enum';
import { FOLLOWUPS, ENTRY } from './src/mock-data/followup';
import { environment } from '../../environments/environment';
import { Errors } from './src/service-errors/entry-followup.enum';



describe('CalendarEntryFollowupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CalendarEntryFollowupService]
    });
  });

  it('should be created', () => {
    const service: CalendarEntryFollowupService = TestBed.get(CalendarEntryFollowupService);
    expect(service).toBeTruthy();
  });

  describe('followup method', () => {
    beforeEach(function() {
      this.service = TestBed.get(CalendarEntryFollowupService);
      this.service.updateEntry(ENTRY);
    });

    it('should get the first page of the calendar entry followups', function(done) {
      const source = CalendarEntryFollowupSource.DUCK_DUCK_GO;
      const page = 0;
      const pageSize = 5;
      this.service.followup(source, page, pageSize).subscribe((data) => {
        let counter = 0;
        for(let i in data) {
          if(data[i].title == FOLLOWUPS[i].title) {
            counter += 1;
          }
        }
        expect(counter).toEqual(5);
        done();
      });
      const httpMock: HttpTestingController = TestBed.get(HttpTestingController);
      const url = new URL(environment.data_center.protocol + '://' + environment.data_center.host);
      url.port = String(environment.data_center.port);
      url.pathname = environment.data_center.routing.economic_news_calendar_entry_followup;
      url.pathname = url.pathname.concat(`/${source}`);
      url.pathname = url.pathname.concat(`/${ENTRY.title().replace(/\//g, '')}`);
      url.pathname = url.pathname.concat(`/${moment(ENTRY.date()).format('YYYY-MM-DD')}`);
      url.pathname = url.pathname.concat(`/${page}`);
      url.pathname = url.pathname.concat(`/${pageSize}`);
      const followupsRequest = httpMock.expectOne(url.toString());
      let counter = 0;
      let result = JSON.parse('{}');
      for(let i in FOLLOWUPS) {
        result[i] = FOLLOWUPS[i];
        counter++;
        if(counter > 4) {
          break;
        }
      }
      followupsRequest.flush(result);
      httpMock.verify();
    });

    it('should get the second page of the calendar entry followups', function(done) {
      const source = CalendarEntryFollowupSource.DUCK_DUCK_GO;
      const page = 1;
      const pageSize = 3;
      this.service.followup(source, page, pageSize).subscribe((data) => {
        let counter = 0;
        for(let i in data) {
          if(data[i].title == FOLLOWUPS[i].title && Number(i) > 2) {
            counter += 1;
          }
        }
        expect(counter).toEqual(3);
        done();
      });
      const httpMock: HttpTestingController = TestBed.get(HttpTestingController);
      const url = new URL(environment.data_center.protocol + '://' + environment.data_center.host);
      url.port = String(environment.data_center.port);
      url.pathname = environment.data_center.routing.economic_news_calendar_entry_followup;
      url.pathname = url.pathname.concat(`/${source}`);
      url.pathname = url.pathname.concat(`/${ENTRY.title().replace(/\//g, '')}`);
      url.pathname = url.pathname.concat(`/${moment(ENTRY.date()).format('YYYY-MM-DD')}`);
      url.pathname = url.pathname.concat(`/${page}`);
      url.pathname = url.pathname.concat(`/${pageSize}`);
      const followupsRequest = httpMock.expectOne(url.toString());
      let counter = 0;
      let pass = 3;
      let result = JSON.parse('{}');
      for(let i in FOLLOWUPS) {
        if(pass > 0) {
          pass--;
        } else {
          result[i] = FOLLOWUPS[i];
          counter++;
          if(counter > 2) {
            break;
          }
        }
      }
      followupsRequest.flush(result);
      httpMock.verify();
    });

    describe('source argument', () => {
      beforeEach(function() {
        this.service = TestBed.get(CalendarEntryFollowupService);
        this.service.updateEntry(ENTRY);
      });

      it('should reject with an error when the source is null', function(done) {
        this.service.followup(null, 0, 5).subscribe((data) => {},
          (err) => {
          expect(err).toContain(Errors.ENTRY_FOLLOWUP_SOURCE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the source is undefined', function(done) {
        this.service.followup(undefined, 0, 5).subscribe((data) => {},
          (err) => {
          expect(err).toContain(Errors.ENTRY_FOLLOWUP_SOURCE_ILLEGAL);
          done();
        });
      });
    });

    describe('page argument', () => {
      beforeEach(function() {
        this.service = TestBed.get(CalendarEntryFollowupService);
        this.service.updateEntry(ENTRY);
      });

      it('should reject with an error when the page is null', function(done) {
        this.service.followup(CalendarEntryFollowupSource.DUCK_DUCK_GO, null, 5).subscribe(
          (data) => {},
          (err) => {
            expect(err).toContain(Errors.PAGE_ILLEGAL);
            done();
          }
        );
      });

      it('should reject with an error when the page is undefined', function(done) {
        this.service.followup(CalendarEntryFollowupSource.DUCK_DUCK_GO, undefined, 5).subscribe(
          (data) => {},
          (err) => {
            expect(err).toContain(Errors.PAGE_ILLEGAL);
            done();
          }
        );
      });

      it('should reject with an error when the page has an illegal value', function(done) {
        this.service.followup(CalendarEntryFollowupSource.DUCK_DUCK_GO, -1, 5).subscribe(
          (data) => {},
          err => {
            expect(err).toContain(Errors.PAGE_ILLEGAL);
            done();
          }
        );
      })
    });

    describe('pageSize argument', () => {
      beforeEach(function() {
        this.service = TestBed.get(CalendarEntryFollowupService);
        this.service.updateEntry(ENTRY);
      });

      it('should reject with an error when the pageSize is null', function(done) {
        this.service.followup(CalendarEntryFollowupSource.DUCK_DUCK_GO, 0, null).subscribe(
          (data) => {},
          (err) => {
            expect(err).toContain(Errors.PAGE_SIZE_ILLEGAL);
            done();
          }
        );
      });

      it('should reject with an error when the pageSize is undefined', function(done) {
        this.service.followup(CalendarEntryFollowupSource.DUCK_DUCK_GO, 0, undefined).subscribe(
          (data) => {},
          (err) => {
            expect(err).toContain(Errors.PAGE_SIZE_ILLEGAL);
            done();
          }
        );
      });

      it('should reject with an error when the pageSize has an illegal value', function(done) {
        this.service.followup(CalendarEntryFollowupSource.DUCK_DUCK_GO, 0, 0).subscribe(
          (data) => {},
          (err) => {
            expect(err).toContain(Errors.PAGE_SIZE_ILLEGAL);
            done();
          }
        );
      });
    });
  });

  describe('updateEntry method', () => {
    it('should throw an eror when the entry is missing', () => {
      const service = TestBed.get(CalendarEntryFollowupService);
      expect(() => {
        service.updateEntry();
      }).toThrowError(Errors.ENTRY_ILLEGAL);
    });

    it('should throw an error when the entry is null', () => {
      const service = TestBed.get(CalendarEntryFollowupService);
      expect(() => {
        service.updateEntry(null);
      }).toThrowError(Errors.ENTRY_ILLEGAL);
    });
  });

  describe('subject method', () => {
    beforeEach(function() {
      this.service = TestBed.get(CalendarEntryFollowupService);
    });
    
    it('should throw an error when the entry is not set', function() {
      expect(() => {
        this.service.followupSubject();
      }).toThrowError(Errors.ENTRY_ILLEGAL);
    });

    it('should return a subject', function() {
      this.service.updateEntry(ENTRY);
      expect(this.service.followupSubject()).toEqual(ENTRY.title());
    });
  });
});
