import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

import { CalendarEntry } from './src/calendar-entry/entry';
import { CalendarEntryFollowupServiceValidator } from './src/service-validator/entry-followup';
import { CalendarEntryFollowupSource } from './src/followup-source/source.enum';
import { environment } from '../../environments/environment';
import { Errors } from './src/service-errors/entry-followup.enum'

import { FOLLOWUPS_RAW } from './src/mock-data/followup';

@Injectable({
  providedIn: 'root'
})
export class CalendarEntryFollowupService {

  private _entry: CalendarEntry;
  private _name: String;
  private _validator: CalendarEntryFollowupServiceValidator;

  constructor(private http: HttpClient) {
    this._validator = new CalendarEntryFollowupServiceValidator();
    this._name = Object.getPrototypeOf(this).constructor.name;
  }

  private areEconomicCalendarEntryFollowupUrlArgsLegal(
    source: CalendarEntryFollowupSource,
    title: String,
    page: Number,
    pageSize: Number,
    date: moment.Moment
  ): void {
    if(!this.validator().followupSourceLegal(source)) {
      throw new Error(Errors.ENTRY_FOLLOWUP_SOURCE_ILLEGAL);
    }
    if(!this.validator().titleLegal(title.toString())) {
      // Should not get here at all.
      // Illegal title is to be identified at the entry object's set method.
      throw new Error(Errors.TITLE_ILLEGAL)
    }
    if(!this.validator().pageLegal(page)) {
      throw new Error(Errors.PAGE_ILLEGAL);
    }
    if(!this.validator().pageSizeLegal(pageSize)) {
      throw new Error(Errors.PAGE_SIZE_ILLEGAL);
    }
    if(!this.validator().dateLegal(date)) {
      // Should not get here at all.
      // Illegal data is to be identified at the entry object's set method.
      throw new Error(Errors.DATE_ILLEGAL);
    }
  }

  private economicCalendarEntryFollowupUrl(
    source: CalendarEntryFollowupSource,
    title: String,
    page: Number,
    pageSize: Number,
    date: moment.Moment
  ): URL {
    this.areEconomicCalendarEntryFollowupUrlArgsLegal(source, title, page, pageSize, date);
    const url = new URL(environment.data_center.protocol + '://' + environment.data_center.host);
    url.port = String(environment.data_center.port);
    url.pathname = environment.data_center.routing.economic_news_calendar_entry_followup;
    url.pathname = url.pathname.concat(`/${source}`);
    url.pathname = url.pathname.concat(`/${title.replace(/\//g, '')}`);
    url.pathname = url.pathname.concat(`/${date.format('YYYY-MM-DD')}`);
    url.pathname = url.pathname.concat(`/${page}`);
    url.pathname = url.pathname.concat(`/${pageSize}`);
    return url;
  }

  followup(source: CalendarEntryFollowupSource, page: Number, pageSize: Number): Observable<JSON> {
    try {
      const url = this.economicCalendarEntryFollowupUrl(
        source,
        this._entry.title(),
        page,
        pageSize,
        moment(this._entry.date())
      );
      return this.http.get<JSON>(url.toString(), { responseType: 'json' });
    } catch(err) {
      return throwError(`${this._name} has failed to execute the method followup(). Reason:\n\t${err.message}`);
    }
    // return this.followupMockData(page, pageSize);
  }
  
  followupNoLongerNeeded(): void {
    if (this._entry) {
      this._entry = null;
    }
  }

  followupSubject(): string {
    if(this._entry) {
      return this._entry.title();
    } else {
      throw new Error(Errors.ENTRY_ILLEGAL);
    }
  }

  updateEntry(entry: CalendarEntry): void {
    // updates the calendar entry that needs the followup
    if(this.validator().entryLegal(entry)) {
      this._entry = entry;
    } else {
      throw new Error(Errors.ENTRY_ILLEGAL);
    }
  }

  private followupMockData(page: Number, pageSize: Number): Observable<JSON> {
    if (this._entry) {
      let result = JSON.parse('{}');
      let mockResponse = JSON.parse(FOLLOWUPS_RAW);
      let skipItems = page.valueOf() * pageSize.valueOf();
      if(skipItems >= Object.keys(mockResponse).length) {
        skipItems = 0;
      }
      let addItems = pageSize.valueOf();
      for(let k in mockResponse) {
        if(skipItems > 0) {
          skipItems = skipItems - 1;
        } else if(addItems > 0){
          addItems = addItems - 1;
          result[k] = mockResponse[k];
        } else {
          break;
        }
      }
      return of<JSON>(result);
    } else {
      return throwError(Errors.ENTRY_ILLEGAL);
    }
  }

  private validator(): CalendarEntryFollowupServiceValidator {
    return this._validator;
  }
}
