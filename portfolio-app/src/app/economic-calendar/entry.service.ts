import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

import { CalendarEntryServiceValidator } from './src/service-validator/entry';
import { CalendarSource } from './src/calendar-source/source.enum';
import { environment } from '../../environments/environment';
import { Errors } from './src/service-errors/entry.enum';

import { ENTRIES_RAW } from './src/mock-data/calendar-entries';

@Injectable({
  providedIn: 'root'
})
export class CalendarEntryService {

  private _name: String;
  private _validator: CalendarEntryServiceValidator;

  constructor( private _http: HttpClient ) {
    this._validator = new CalendarEntryServiceValidator();
    this._name = Object.getPrototypeOf(this).constructor.name;
  }

  private areEconomicCalendarEntriesUrlArgsLegal(
    source: CalendarSource,
    startDate: moment.Moment,
    endDate: moment.Moment,
    page: Number,
    pageSize: Number
  ): void {
    if(!this.validator().calendarSourceLegal(source)) {
      throw new Error(Errors.SOURCE_ILLEGAL);
    }
    if(!this.validator().dateLegal(startDate) || !this.validator().dateLegal(endDate)) {
      throw new Error(Errors.DATE_ILLEGAL);
    }
    if(!this.validator().pageLegal(page)) {
      throw new Error(Errors.PAGE_ILLEGAL);
    }
    if(!this.validator().pageSizeLegal(pageSize)) {
      throw new Error(Errors.PAGE_SIZE_ILLEGAL)
    }
  }

  private economicCalendarEntriesUrl(
    source: CalendarSource,
    startDate: moment.Moment,
    endDate: moment.Moment,
    page: Number,
    pageSize: Number
  ): URL {
    this.areEconomicCalendarEntriesUrlArgsLegal(source, startDate, endDate, page, pageSize);
    const url = new URL(environment.data_center.protocol + '://' + environment.data_center.host);
    url.port = String(environment.data_center.port);
    url.pathname = environment.data_center.routing.economic_news_calendar_entries;
    url.pathname = url.pathname.concat(`/${source}`);
    url.pathname = url.pathname.concat(`/${startDate.format('YYYY-MM-DD')}`);
    url.pathname = url.pathname.concat(`/${endDate.format('YYYY-MM-DD')}`);
    url.pathname = url.pathname.concat(`/${page}/${pageSize}`);
    return url;
  }

  entries(
    source: CalendarSource,
    page: Number,
    pageSize: Number,
    startDate: moment.Moment,
    endDate: moment.Moment
  ): Observable<JSON> {
    try {
      const url = this.economicCalendarEntriesUrl(source, startDate, endDate, page, pageSize);
      return this._http.get<JSON>(url.toString(), {
        responseType: 'json'
      });
    } catch(err) {
      return throwError(`${this._name} has failed to execute the method entries(). Reason:\n\t${err.message}`)
    }
    // return this.entriesMockData(page, pageSize);
  }

  private entriesMockData(page: Number, pageSize: Number): Observable<JSON> {
    let result = JSON.parse('{}');
    let mockResponse = JSON.parse(ENTRIES_RAW);
    let skipItems = page.valueOf() * pageSize.valueOf();
    if(skipItems >= Object.keys(mockResponse).length) {
      skipItems = 0;
    }
    let addItems = pageSize.valueOf();
    for(let k in mockResponse) {
      if(skipItems > 0) {
        skipItems = skipItems - 1;
      } else if(addItems > 0){
        addItems = addItems - 1;
        result[k] = mockResponse[k];
      } else {
        break;
      }
    }
    return of<JSON>(result);
  }

  private validator(): CalendarEntryServiceValidator {
    return this._validator;
  }
}
