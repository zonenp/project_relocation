import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';

import { CalendarEntryService } from './entry.service';
import { CalendarSource } from './src/calendar-source/source.enum';
import { environment } from '../../environments/environment';
import { Errors } from './src/service-errors/entry.enum';
import { ENTRIES } from './src/mock-data/calendar-entries';

describe('CalendarEntryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CalendarEntryService]
    });
  });

  it('should be created', () => {
    const service: CalendarEntryService = TestBed.get(CalendarEntryService);
    expect(service).toBeTruthy();
  });

  describe('entries method', () => {
    it('should get the first page of the calendar entries', (done) => {
      const service: CalendarEntryService = TestBed.get(CalendarEntryService);
      const source: CalendarSource = CalendarSource.FOREX_FACTORY;
      const page: Number = 0;
      const pageSize: Number = 5;
      const startDate = moment(new Date());
      const endDate = moment(new Date());
      service.entries(source, page, pageSize, startDate, endDate).subscribe((data) => {
        let counter = 0;
        for(let i in data) {
          if(data[i].title() == ENTRIES[i].title()) {
            counter += 1;
          }
        }
        expect(counter).toEqual(5);
        done();
      });
      const httpMock: HttpTestingController = TestBed.get(HttpTestingController);
      const url = new URL(environment.data_center.protocol + '://' + environment.data_center.host);
      url.port = String(environment.data_center.port);
      url.pathname = environment.data_center.routing.economic_news_calendar_entries;
      url.pathname = url.pathname.concat(`/${source}`);
      url.pathname = url.pathname.concat(`/${startDate.format('YYYY-MM-DD')}`);
      url.pathname = url.pathname.concat(`/${endDate.format('YYYY-MM-DD')}`);
      url.pathname = url.pathname.concat(`/${page}/${pageSize}`);
      const entriesRequest = httpMock.expectOne(url.toString());
      let counter = 0;
      let result = JSON.parse('{}');
      for(let i in ENTRIES) {
        result[i] = ENTRIES[i];
        counter++;
        if(counter > 4) {
          break;
        }
      }
      entriesRequest.flush(result);
      httpMock.verify();
    });
  
    it('should get the second page of the calendar entries', (done) => {
      const service: CalendarEntryService = TestBed.get(CalendarEntryService);
      const source: CalendarSource = CalendarSource.FOREX_FACTORY;
      const page: Number = 1;
      const pageSize: Number = 3;
      const startDate = moment(new Date());
      const endDate = moment(new Date());
      service.entries(source, page, pageSize, startDate, endDate).subscribe((data) => {
        let counter = 0;
        for(let i in data) {
          if(data[i].title() == ENTRIES[i].title() && Number(i) > 2) {
            counter += 1;
          }
        }
        expect(counter).toEqual(3);
        done();
      });
      const httpMock: HttpTestingController = TestBed.get(HttpTestingController);
      const url = new URL(environment.data_center.protocol + '://' + environment.data_center.host);
      url.port = String(environment.data_center.port);
      url.pathname = environment.data_center.routing.economic_news_calendar_entries;
      url.pathname = url.pathname.concat(`/${source}`);
      url.pathname = url.pathname.concat(`/${startDate.format('YYYY-MM-DD')}`);
      url.pathname = url.pathname.concat(`/${endDate.format('YYYY-MM-DD')}`);
      url.pathname = url.pathname.concat(`/${page}/${pageSize}`);
      const entriesRequest = httpMock.expectOne(url.toString());
      let counter = 0;
      let pass = 3;
      let result = JSON.parse('{}');
      for(let i in ENTRIES) {
        if(pass > 0) {
          pass--;
        } else {
          result[i] = ENTRIES[i];
          counter++;
          if(counter > 2) {
            break;
          }
        }
      }
      entriesRequest.flush(result);
      httpMock.verify();
    });
  
    describe('source argument', () => {
      it('should reject with an error when the source is null', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(null, 0, 5, moment(new Date), moment(new Date)).subscribe(
          (data) => {},
          (err) => {
          expect(err).toContain(Errors.SOURCE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the source is undefined', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(undefined, 0, 5, moment(new Date), moment(new Date)).subscribe(
          (data) => {},
          (err) => {
          expect(err).toContain(Errors.SOURCE_ILLEGAL);
          done();
        });
      });
    });
  
    describe('page argument', () => {
      it('should reject with an error when the page is null', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(CalendarSource.FOREX_FACTORY, null, 5, moment(new Date), moment(new Date))
        .subscribe((data) => {}, (err) => {
          expect(err).toContain(Errors.PAGE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the page is undefined', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(
          CalendarSource.FOREX_FACTORY,
          undefined,
          5,
          moment(new Date),
          moment(new Date)
        ).subscribe((data) => {}, (err) => {
          expect(err).toContain(Errors.PAGE_ILLEGAL);
          done();
        });
      });
  
      it('should reject with an error when the page has illegal value', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(CalendarSource.FOREX_FACTORY, -1, 5, moment(new Date), moment(new Date))
        .subscribe((data) => {}, (err) => {
          expect(err).toContain(Errors.PAGE_ILLEGAL);
          done();
        });
      });
    });
  
    describe('pageSize argument', () => {
      it('should reject with an error when the pageSize is null', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(CalendarSource.FOREX_FACTORY, 0, null, moment(new Date), moment(new Date))
        .subscribe((data) => {}, (err) => {
          expect(err).toContain(Errors.PAGE_SIZE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the pageSize is undefined', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(
          CalendarSource.FOREX_FACTORY,
          0,
          undefined,
          moment(new Date),
          moment(new Date)
        ).subscribe((data) => {}, (err) => {
          expect(err).toContain(Errors.PAGE_SIZE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the pageSize has illegal value', function(done) {
        const service: CalendarEntryService = TestBed.get(CalendarEntryService);
        service.entries(CalendarSource.FOREX_FACTORY, 0, -1, moment(new Date), moment(new Date))
        .subscribe((data) => {}, (err) => {
          expect(err).toContain(Errors.PAGE_SIZE_ILLEGAL);
          done();
        });
      });
    });
  });
});
