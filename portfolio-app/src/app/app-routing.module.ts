import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreditsBarComponent } from './credits-bar/credits-bar.component';
import { EconomicCalendarComponent } from './economic-calendar/economic-calendar.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';


const routes: Routes = [
  {
    path: '', redirectTo: 'welcome_page', pathMatch: 'full'
  },
  {
    path: 'welcome_page', component: WelcomePageComponent
  },
  {
    path: 'economic_calendar', component: EconomicCalendarComponent
  },
  {
    path: 'dev_credits_bar', component: CreditsBarComponent
  },
  {
    path: 'dev_navigation_bar', component: NavigationBarComponent
  },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
