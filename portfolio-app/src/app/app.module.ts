import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { CreditsBarComponent } from './credits-bar/credits-bar.component';
import { EconomicCalendarComponent } from './economic-calendar/economic-calendar.component';
import { ShortLinkTextPipe } from './short-link-text.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    CreditsBarComponent,
    PageNotFoundComponent,
    WelcomePageComponent,
    EconomicCalendarComponent,
    ShortLinkTextPipe
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
