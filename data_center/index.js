const cors = require('cors');
const express = require('express');

import { config } from './config/config.dev.js';
import { economicCalendarEntries } from './lib/economic_news/calendar_entries/api';
import { economicEntryFollowups } from './lib/economic_news/followups/api';
import { Logger } from './lib/logger/logger';
import { ServerAlive } from './lib/monitor/server_alive';

const app = express();
app.use(cors());

const logger = new Logger({
  env: config,
  level: 'debug', 
  logStream: config.log.log_stream
});

app.get('/ping', function(req, res) {
  let message = new ServerAlive({
    logger: logger
  }).pong();
  res.writeHead(200, { 'Content-type': 'text/plain' });
  res.end(message);
});

app.get('/economic_news/calendar_entries/:source/:start_date/:end_date/:page/:page_size', function(req, res) {
  economicCalendarEntries({
    env: config,
    logger: logger,
    params: req.params
  }).then((data) => {
    res.status(200);
    res.end(JSON.stringify(data));
  }).catch((err) => {
    logger.error('router', err.message);
    res.writeHead(500, { 'Content-type': 'text/plain' });
    res.end(JSON.stringify({
      message: 'Failed to fetch the economic calendar entries',
      reason: err.message
    }));
  });
});

app.get('/economic_news/followups/:source/:title/:date/:page/:page_size', function(req, res) {
  economicEntryFollowups({
    env: config,
    logger: logger,
    params: req.params
  }).then((data) => {
    res.status(200);
    res.end(JSON.stringify(data));
  }).catch((err) => {
    logger.error('router', err.message);
    res.writeHead(500, { 'Content-type': 'text/plain' });
    res.end(JSON.stringify({
      message: 'Failed to fetch the economic entry followups',
      reason: err.message
    }));
  });
});

app.listen(config.port, () => {
  logger.info(Object.getPrototypeOf(app).constructor.name, `Listenning to requests on ${config.port}`);
});