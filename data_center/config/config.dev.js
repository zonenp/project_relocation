const path = require('path');

const config = {
  port: 3000,
  economic_calendar: {
    forex_factory: {
      host: 'cdn-nfs.faireconomy.media',
      path: '/ff_calendar_thisweek.json?date='
    }
  },
  log: {
    date_pattern: 'YYYY-MM-DD-HH',
    debug: path.join(__dirname, '..', 'log', 'debug-%DATE%.log'),
    info: path.join(__dirname, '..', 'log', 'info-%DATE%.log'),
    log_stream: 'console',
    max_files: '3',
    max_size: '5m',
    zip: true
  },
  miner: {
    duckduckgo: {
      accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
      accept_encoding: 'gzip, deflate, br',
      accept_language: 'en-US,en;q=0.5',
      agent: 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0',
      connection: 'keep-alive',
      host: 'duckduckgo.com',
      path: '/html',
      response_asset: path.join(__dirname, '..', 'assets', 'response_duckduckgo.html'),
      te: 'Trailers',
      upgrade_insecure_requests: '1'
    }
  }
}

export { config }