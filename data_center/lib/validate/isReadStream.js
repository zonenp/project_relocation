function isReadStream(obj) {
  try {
    return Object.getPrototypeOf(obj).constructor.name === 'ReadStream';
  } catch(e) { return false; }
}

export { isReadStream };