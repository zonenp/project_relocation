function isInteger(value) {
 try {
   const minimal = 0;
   const expectedType = Object.getPrototypeOf(minimal).constructor.name;
   const actualType = Object.getPrototypeOf(value).constructor.name;
   if(expectedType === actualType) {
    return value >= minimal;
   } else {
     return false;
   }
 } catch(e) { return false; }
}

export { isInteger };