function isDate(value) {
  if(value) {
    try {
      const now = new Date();
      const expectedType = Object.getPrototypeOf(now).constructor.name;
      const actualType = Object.getPrototypeOf(value).constructor.name;
      return  expectedType === actualType;
    } catch(e) { return false; }
  } else { return false; }
}
 
 export { isDate };