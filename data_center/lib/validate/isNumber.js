function isNumber(value) {
  try {
    return Object.getPrototypeOf(value).constructor.name == 'Number';
  } catch(err) {
    return false;
  }
}

export { isNumber }