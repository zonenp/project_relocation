function isString(obj) {
  try {
    return Object.getPrototypeOf(obj).constructor.name === 'String';
  } catch(e) {
    return false;
  }
}

export { isString }