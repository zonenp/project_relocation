export { isNonEmptyString } from './isNonEmptyString';

function isLink(value) {
  try {
    if (isNonEmptyString(value)) {
      const regex = new RegExp(
        '^(http|https):\\/\\/[(www.)]*[a-z0-9:]+.[a-z0-9][\\/a-zA-Z0-9?=&\\-%\\._]+$'
      );
      if(regex.test(value)) {
        return true;
      }
    }
    return false;
  } catch(e) { return false; }
 }
 
 export { isLink };