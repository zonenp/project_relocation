import { isString } from './isString';

function isNonEmptyString(obj) {
  if (isString(obj)) {
    try {
      return obj.length > 0;
    } catch(error) {
      return false;
    }

  }
}

export { isNonEmptyString }