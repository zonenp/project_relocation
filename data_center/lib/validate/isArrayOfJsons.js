import { isNonEmptyJson } from './isNonEmptyJson';

function isArrayOfJsons(value) {
  let legal = false;
  if(value) {
    for(let obj in value) {
      legal = isNonEmptyJson(value[obj]);
      if(!legal) { break; }
    }
  }
  return legal;
}

export { isArrayOfJsons };