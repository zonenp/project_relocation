import { isNonEmptyString } from './isNonEmptyString';

function isRawDate(obj) {
  if(isNonEmptyString(obj)) {
    return !(new Date(obj) == 'Invalid Date');
  }
  return false;
}

  export { isRawDate }