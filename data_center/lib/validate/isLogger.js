function isLogger(obj) {
  try {
    return Object.getPrototypeOf(obj).constructor.name == 'Logger';
  } catch(err) {
    return false;
  }
}

export { isLogger }