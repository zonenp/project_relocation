function isNonEmptyJson(obj) {
  try {
    if(Object.getPrototypeOf(obj).constructor.name === 'String') { return false; }
    if(Object.getPrototypeOf(obj).constructor.name === 'Array') { return false; }
    let count = 0;
    for(let k in obj) {
      if(obj[k]) {
        count++;
        break;
      }
    }
    if(Object.getPrototypeOf(obj).constructor.name === 'Object' && count > 0) { return true; }
    return false;
  } catch(e) { return false; }
}

export { isNonEmptyJson }