import { DuckDuckGoMiner } from './src/duck_duck_go';
import { Errors } from './src/errors';
import { FollowupSource } from './src/source';

async function economicEntryFollowups(args) {
  // TODO: finish the tests
  if(args && args.params && args.params.source) {
    switch(args.params.source) {
      case FollowupSource.DUCK_DUCK_GO:
        let miner = undefined;
        try {
          miner = new DuckDuckGoMiner({
            env: args.env,
            logger: args.logger
          }); 
        } catch(err) {
          return new Promise((res, rej) => {
            rej(new Error(Errors.DUCK_DUCK_GO_MINER_INIT_FAILED));
          });
        }
        let date = undefined;
        try {
          if(args.params.date) {
            date = new Date(args.params.date);
          } else {
            throw new Error();
          }
        } catch(err) {
          return new Promise((res, rej) => {
            rej(new Error(Errors.OBJECT_IS_NOT_A_DATE));
          });
        }
        return miner.links(
          args.params.title,
          date,
          parseInt(args.params.page),
          parseInt(args.params.page_size)
        );
      default:
        return new Promise((res, rej) => {
          rej(new Error(Errors.FOLLOWUP_SOURCE_IS_ILLEGAL));
        });
    }
  } else {
    return new Promise((res, rej) => {
      rej(new Error(Errors.ARGS_TO_API_ARE_ILLEGAL));
    })
  }
}

export { economicEntryFollowups }