const expect = require('chai').expect;
const readline = require('readline');
const fs = require('fs');
const path = require('path');

import { config } from '../../../../config/config.dev';
import { DuckDuckGoMiner } from '../src/duck_duck_go';
import { Errors } from '../src/errors';
import { isNonEmptyString } from '../../../validate/isNonEmptyString';
import { Logger } from '../../../logger/logger';
import { LogStream } from '../../../logger/src/stream';

describe('DuckDuckGoMiner', () => {
  describe('env argument', function() {
    before(function() {
      this.logger = new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE });
    });

    it('should throw an error when the env is missing', function() {
      expect(() => { new DuckDuckGoMiner({
        logger: this.logger
      }) }).to.throw(Errors.ENV_IS_ILLEGAL);
    });

    it('should throw an error when the env is null', function() {
      expect(() => { new DuckDuckGoMiner({
        env: null,
        logger: this.logger
      }) }).to.throw(Errors.ENV_IS_ILLEGAL);
    });

    it('should throw an error when the env is undefined', function() {
      expect(() => { new DuckDuckGoMiner({
        env: undefined,
        logger: this.logger
      }) }).to.throw(Errors.ENV_IS_ILLEGAL);
    });

    it('should throw an error when the env is of illegal type', function() {
      expect(() => { new DuckDuckGoMiner({
        env: '{"key": "value"}',
        logger: this.logger
      }) }).to.throw(Errors.ENV_IS_ILLEGAL);
    });
  });

  describe('logger argument', () => {
    it('should throw an error when the logger is missing', () => {
      expect(() => { new DuckDuckGoMiner({ env: config })}).to.throw(Errors.LOGGER_IS_ILLEGAL);
    });

    it('should throw an error when the logger is null', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: null
      })}).to.throw(Errors.LOGGER_IS_ILLEGAL);
    });

    it('should throw an error when the logger is undefined', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: undefined
      })}).to.throw(Errors.LOGGER_IS_ILLEGAL);
    });
  });

  it('should create an object', () => {
    const miner = new DuckDuckGoMiner({
      env: config,
      logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
    });
    expect(miner.constructor.name).to.equal('DuckDuckGoMiner');
  });

  describe('_rawData()', () => {
    before(function() {
      config.miner.duckduckgo.response_asset = path.join(
        __dirname, 'response_duckduckgo_test.html'
      );
      this.miner = new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.legalTitle = 'European Parliamentary Elections';
      this.legalDate = new Date('2019-05-26T06:16:00-04:00');
      this.legalPage = 1;
      this.legalPageSize = 2;
    });
    
    it('should reject with an error when the title is null', function(done) {
      this.miner._rawData(null, this.legalDate)
      .catch((err) => {
        expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
        done();
      });
    });
    
    it('should reject with an error when the title is undefined', function(done) {
      this.miner._rawData(undefined, this.legalDate)
      .catch((err) => {
        expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
        done();
      });
    });
    
    it('should reject with an error when the title is empty', function(done) {
      this.miner._rawData('', this.legalDate)
      .catch((err) => {
        expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
        done();
      });
    });

    it('should reject with an error when the title is of illegal type', function(done) {
      this.miner._rawData(123, this.legalDate)
      .catch((err) => {
        expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
        done();
      });
    });

    it('should reject with an error when the date is null', function(done) {
      this.miner._rawData(this.legalTitle, null)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_DATE);
          done();
        });
    });

    it('should reject with an error when the date is undefined', function(done) {
      this.miner._rawData(this.legalTitle, undefined)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_DATE);
          done();
        });
    });

    it('should reject with an error when the date is of illegal type', function(done) {
      this.miner._rawData(this.legalTitle, '2019-09-09')
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_DATE);
          done();
        });
    });

    it('should reject with an error when the title is null', function(done) {
      this.miner._rawData(null, this.legalDate)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
          done();
        });
    });

    it('should reject with an error when the title is undefined', function(done) {
      this.miner._rawData(undefined, this.legalDate)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
          done();
        });
    });

    it('should reject with an error when the title is of the wrong type', function(done) {
      this.miner._rawData(1, this.legalDate)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
          done();
        });
    });

    it('should retrieve and decrypt search results into a file', function(done) {
      try {
        fs.unlinkSync(config.miner.duckduckgo.response_asset);
      } catch(e) {
        // pass
      }
      this.miner._rawData(this.legalTitle, this.legalDate)
        .then((data) => {
          let line = undefined;
          const readLineApi = readline.createInterface({
            input: data
          });
          readLineApi.on('line', (l) => {
            if(line) {
              // pass
            } else {
              line = l;
              readLineApi.close();
            }
          })
          readLineApi.on('close', () => {
            if(line) {
              expect(line).to.have.string('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0');
              done();
            }
          });
        });
    });
  });

  describe('_parseRawData()', () => {
    before(function() {
      config.miner.duckduckgo.response_asset = path.join(
        __dirname, 'response_duckduckgo_test.html'
      );
      this.miner = new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.legalTitle = 'European Parliamentary Elections';
      this.legalDate = new Date('2019-05-26T06:16:00-04:00');
    });

    it('should reject with an error when the raw data is null', function(done) {
      this.miner._parseRawData(null)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_STREAM_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the raw data is undefined', function(done) {
      this.miner._parseRawData(null)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_STREAM_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the raw data has illegal value', function(done) {
      this.miner._parseRawData('')
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_STREAM_IS_ILLEGAL);
          done();
        });
    });

    it('should resolve with a collection<JSON> when the data is legal', function(done) {
      this.miner._rawData(this.legalTitle, this.legalDate)
        .then((input) => {
          this.miner._parseRawData(input)
            .then((data) => {
              if(isNonEmptyString(data[0].title)) {
                done();
              }
            });
        });
    });
  });

  describe('_filterData()', () => {
    before(function() {
      this.miner = new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.legalData = [
        JSON.parse('{"name": "one", "link": "jj"}'),
        JSON.parse('{"name": "two", "link": "jj"}')
      ]
      this.legalPage = 1;
      this.legalPageSize = 2;
    });

    it('should reject with an error when the data is null', function(done) {
      this.miner._filterData(null, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the data is undefined', function(done) {
      this.miner._filterData(undefined, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the data is of the wrong type', function(done) {
      this.miner._filterData(JSON.stringify('{"one": 2}'), this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the page is null', function(done) {
      this.miner._filterData(this.legalData, null, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the page is undefined', function(done) {
      this.miner._filterData(this.legalData, undefined, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the page is of the wrong type', function(done) {
      this.miner._filterData(this.legalData, "1", this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the page has illegal value', function(done) {
      this.miner._filterData(this.legalData, -1, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize is null', function(done) {
      this.miner._filterData(this.legalData, this.legalPage, null)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize is undefined', function(done) {
      this.miner._filterData(this.legalData, this.legalPage, undefined)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize is of the wrong type', function(done) {
      this.miner._filterData(this.legalData, this.legalPage, "2")
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize has illegal value', function(done) {
      this.miner._filterData(this.legalData, this.legalPage, 0)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });
  });

  describe('_fillInPage()', () => {
    before(function() {
      this.miner = new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.legalData = [
        JSON.parse('{"name": "one", "link": "jj"}'),
        JSON.parse('{"name": "two", "link": "jj"}'),
        JSON.parse('{"name": "three", "link": "kuku"}')
      ]
      this.legalPageSize = 1;
      this.legalPassElements = 1;
    });

    it('should reject with an error when the data is null', function(done) {
      this.miner._fillInPage(null, this.legalPageSize, this.legalPassElements)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the data is undefined', function(done) {
      this.miner._fillInPage(undefined, this.legalPageSize, this.legalPassElements)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the data is of the wrong type', function(done) {
      this.miner._fillInPage(JSON.parse('{"name": 2}'), this.legalPageSize, this.legalPassElements)
        .catch((err) => {
          expect(err.message).to.equal(Errors.DATA_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize is null', function(done) {
      this.miner._fillInPage(this.legalData, null, this.legalPassElements)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize is undefined', function(done) {
      this.miner._fillInPage(this.legalData, undefined, this.legalPassElements)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize is of the wrong type', function(done) {
      this.miner._fillInPage(this.legalData, '2', this.legalPassElements)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the pageSize has illegal value', function(done) {
      this.miner._fillInPage(this.legalData, 0, this.legalPassElements)
        .catch((err) => {
          expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
          done();
        });
    });

    it('should reject with an error when the passElements is null', function(done) {
      this.miner._fillInPage(this.legalData, this.legalPageSize, null)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_AN_INTEGER);
          done();
        });
    });

    it('should reject with an error when the passElements is undefined', function(done) {
      this.miner._fillInPage(this.legalData, this.legalPageSize, undefined)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_AN_INTEGER);
          done();
        });
    });

    it('should reject with an error when the passElements is of the wrong type', function(done) {
      this.miner._fillInPage(this.legalData, this.legalPageSize, '2')
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_AN_INTEGER);
          done();
        });
    });

    it('should reject with an error when the passElements has illegal value', function(done) {
      this.miner._fillInPage(this.legalData, this.legalPageSize, -1)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_AN_INTEGER);
          done();
        });
    });

    it('should return a page of followups', function(done) {
      this.miner._fillInPage(this.legalData, this.legalPageSize, this.legalPassElements)
        .then((data) => {
          for(let idx in data) {
            let followup = data[idx];
            expect(followup['name']).to.equal('two');
          }
          done();
        });
    });
  });

  describe('_dateToQueryParam()', () => {
    it('should throw an error when the date is null', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._dateToQueryParam(null) })
        .to.throw(Errors.OBJECT_NOT_A_DATE);
    });

    it('should throw an error when the date is undefined', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._dateToQueryParam(undefined) })
        .to.throw(Errors.OBJECT_NOT_A_DATE);
    });

    it('should throw an error when the date is of the wrong type', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._dateToQueryParam('2019-09-09') })
        .to.throw(Errors.OBJECT_NOT_A_DATE);
    });

    it('should return a string for a query param when the date is legal', () => {
      expect(new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._dateToQueryParam(new Date('2019-09-09')))
        .to.equal('9+9+2019');
    });
  });

  describe('_titleToQueryParam()', () => {
    it('should throw an error when the title is null', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._titleToQueryParam(null) })
        .to.throw(Errors.OBJECT_NOT_A_TITLE);
    });

    it('should throw an error when the title is undefined', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._titleToQueryParam(undefined) })
        .to.throw(Errors.OBJECT_NOT_A_TITLE);
    });

    it('should throw an error when the title is of the wrong type', () => {
      expect(() => { new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._titleToQueryParam(1) })
        .to.throw(Errors.OBJECT_NOT_A_TITLE);
    });

    it('should return a string with a query param when the title is legal', () => {
      expect(new DuckDuckGoMiner({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      })._titleToQueryParam('Bank of Japan'))
        .to.equal('bank+of+japan');
    });
  });
});