const expect = require('chai').expect;

import { config } from '../../../../config/config.dev';
import { FollowupMiner } from '../src/miner';
import { Errors } from '../src/errors';
import { Logger } from '../../../logger/logger';
import { LogStream } from '../../../logger/src/stream';

describe('FollowupMiner', () => {
  describe('env argument', function() {
    before(function() {
      this.logger = new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE });
    });
    
    it('should throw an error when the env is missing', function() {
      expect(() => { new FollowupMiner({ logger: this.logger }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is null', function() {
      expect(() => { new FollowupMiner({
        env: null,
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is undefined', function() {
      expect(() => { new FollowupMiner({
        env: undefined,
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is of illegal type', function() {
      expect(() => { new FollowupMiner({
        env: '{"key": "value"}',
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is empty', function() {
      expect(() => { new FollowupMiner({
        env: {},
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });
  });

  describe('logger argument', function() {
    it('should throw an error when the logger is missing', function() {
      expect(() => { new FollowupMiner({ env: config }) }).to.throw(Errors.LOGGER_ILLEGAL);
    });

    it('should throw an error when the logger is null', function() {
      expect(() => { new FollowupMiner({
        env: config,
        logger: null
      }) }).to.throw(Errors.LOGGER_ILLEGAL);
    });

    it('should throw an error when the logger is undefined', function() {
      expect(() => { new FollowupMiner({
        env: config,
        logger: undefined
      }) }).to.throw(Errors.LOGGER_ILLEGAL);
    });
  });

  it('should create an object', () => {
    const miner = new FollowupMiner({
      env: config,
      logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
    });
    expect(miner.constructor.name).to.equal('FollowupMiner');
  });

  describe('links method', () => {
    describe('title argument', function() {
      before(function() {
        this.miner = new FollowupMiner({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
        this.legalTitle = 'European Parliamentary Elections';
        this.legalDate = new Date('2019-05-26T06:16:00-04:00');
        this.legalPage = 1;
        this.legalPageSize = 2;
      });

      it('should reject with an error when the title is null', function(done) {
        this.miner.links(null, this.legalDate, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
          done();
        });
      });
      
      it('should reject with an error when the title is undefined', function(done) {
        this.miner.links(undefined, this.legalDate, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
          done();
        });
      });
      
      it('should reject with an error when the title is empty', function(done) {
        this.miner.links('', this.legalDate, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
          done();
        });
      });
  
      it('should reject with an error when the title is of illegal type', function(done) {
        this.miner.links(123, this.legalDate, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_NOT_A_TITLE);
          done();
        });
      });
    });

    describe('date argument', function() {
      before(function() {
        this.miner = new FollowupMiner({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
        this.legalTitle = 'European Parliamentary Elections';
        this.legalDate = new Date('2019-05-26T06:16:00-04:00');
        this.legalPage = 1;
        this.legalPageSize = 2;
      });
      
      it('should reject with an error when the date is null', function(done) {
        this.miner.links(this.legalTitle, null, this.legalPage, this.legalPageSize)
          .catch((err) => {
            expect(err.message).to.equal(Errors.OBJECT_NOT_A_DATE);
            done();
          });
      });
      
      it('should reject an error when the date is undefined', function(done) {
        this.miner.links(this.legalTitle, undefined, this.legalPage, this.legalPageSize)
          .catch((err) => {
            expect(err.message).to.equal(Errors.OBJECT_NOT_A_DATE);
            done();
          });
      });
  
      it('should reject with an error when the date is of illegal type', function(done) {
        this.miner.links(this.legalTitle, '2019-09-09', this.legalPage, this.legalPageSize)
          .catch((err) => {
            expect(err.message).to.equal(Errors.OBJECT_NOT_A_DATE);
            done();
          });
      });
    });

    describe('page argument', function() {
      before(function() {
        this.miner = new FollowupMiner({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
        this.legalTitle = 'European Parliamentary Elections';
        this.legalDate = new Date('2019-05-26T06:16:00-04:00');
        this.legalPage = 1;
        this.legalPageSize = 2;
      });
      
      it('should reject with an error when the page is null', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, null, this.legalPageSize)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
            done();
          });
      });
  
      it('should reject with an error when the page is undefined', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, undefined, this.legalPageSize)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
            done();
          });
      });
  
      it('should reject with an error when the page is of illegal type', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, '2', this.legalPageSize)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
            done();
          });
      });
  
      it('should reject with an error when the page has illegal value', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, -1, this.legalPageSize)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_IS_ILLEGAL);
            done();
          });
      });
    });

    describe('pageSize argument', function() {
      before(function() {
        this.miner = new FollowupMiner({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
        this.legalTitle = 'European Parliamentary Elections';
        this.legalDate = new Date('2019-05-26T06:16:00-04:00');
        this.legalPage = 1;
        this.legalPageSize = 2;
      });
      
      it('should reject with an error when the pageSize is null', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, this.legalPage, null)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
            done();
          });
      });
  
      it('should reject with an error when the pageSize is undefined', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, this.legalPage, undefined)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
            done();
          });
      });
  
      it('should reject with an error when the pageSize is of illegal type', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, this.legalPage, '2')
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
            done();
          });
      });
  
      it('should reject with an error when the pageSize has illegal value', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, this.legalPage, 0)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
            done();
          });
      });
  
      it('should reject with an error when the pageSize is null', function(done) {
        this.miner.links(this.legalTitle, this.legalDate, this.legalPage, null)
          .catch((err) => {
            expect(err.message).to.equal(Errors.PAGE_SIZE_IS_ILLEGAL);
            done();
          });
      });
    });
  });

});