import { Errors } from './errors';
import { FollowupMinerValidator } from './validator';

class FollowupMiner {
  // This is an abstract class

  constructor(args) {
    this._validator = new FollowupMinerValidator();
    this._setEnv(args.env);
    this._setLogger(args.logger);
    this._name = Object.getPrototypeOf(this).constructor.name;
  }

  async links(title, date, page, pageSize) {
    // The caller is responsible for taking care of the error
    this.logger().info(this._name, 'executing the links() method ');
    let debugMsg = 'links() arguments:';
    debugMsg += `\n\ttitle:\t${title},\n\tdate:\t${date},\n\tpage:\t${page},`;
    debugMsg += `\n\tpageSize:\t${pageSize}`;
    this.logger().debug(this._name, debugMsg);
    try {
      this._areLinksArgsLegal(title, date, page, pageSize);    
    } catch(err) {
      return new Promise((res, rej) => {
        rej(err);
      });
    }
    // descendants implementation
  }

  env() {
    return this._env;
  }

  logger() {
    return this._logger;
  }

  validator() {
    return this._validator;
  }

  _areLinksArgsLegal(title, date, page, pageSize) {
    if(!this.validator().isNonEmptyString(title)) {
      throw new Error(Errors.OBJECT_NOT_A_TITLE);
    }
    if(!this.validator().isDate(date)) {
      throw new Error(Errors.OBJECT_NOT_A_DATE);
    }
    if(!this.validator().pageLegal(page)) {
      throw new Error(Errors.PAGE_IS_ILLEGAL);
    }
    if(!this.validator().pageSizeLegal(pageSize)) {
      throw new Error(Errors.PAGE_SIZE_IS_ILLEGAL);
    }
  }

  _setEnv(env) {
    if(this._validator.envLegal(env)) {
      this._env = env;
    } else {
      throw new Error(Errors.ENV_IS_ILLEGAL);
    }
  }

  _setLogger(logger) {
    if(this.validator().loggerLegal(logger)) {
      this._logger = logger;
    } else {
      throw new Error(Errors.LOGGER_IS_ILLEGAL);
    }
  }
}

export { FollowupMiner }