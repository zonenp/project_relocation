const Errors = Object.freeze({
  ARGS_TO_API_ARE_ILLEGAL: 'Args to Economic Entry Followup API are illegal.',
  DATA_IS_ILLEGAL: 'Data is illegal. Expected a __collection<json>__',
  DATA_STREAM_IS_ILLEGAL: 'Data stream is illegal. Expected a __ReadStream__',
  DUCK_DUCK_GO_MINER_INIT_FAILED: 'Failed to initialize a __DuckDuckGoMiner__ object.',
  ENV_IS_ILLEGAL: 'Env is illegal. Expected a non empty __JSON__',
  FOLLOWUP_SOURCE_IS_ILLEGAL: 'Economic Entry Followup Source is illegal. Expected an enum __FollowupSource__',
  LOGGER_IS_ILLEGAL: 'Logger is illegal. Expected a __Logger__',
  OBJECT_NOT_AN_INTEGER: 'Expected an __integer__',
  OBJECT_NOT_A_DATE: 'Date is illegal. Expected a __date__',
  OBJECT_NOT_A_TITLE: 'Title is illegal. Expected a non empty __string__',
  PAGE_IS_ILLEGAL: 'Page is illegal. Expected an __integer__ >= 0',
  PAGE_SIZE_IS_ILLEGAL: 'Page size is illegal. Expected an __integer__ > 0',
  RAW_DATA_NOT_LEGAL: 'Raw data is illegal. Expected a __stringified JSON__'
});

export { Errors }