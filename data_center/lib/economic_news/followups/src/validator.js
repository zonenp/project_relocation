import { isArrayOfJsons } from '../../../validate/isArrayOfJsons';
import { isDate } from '../../../validate/isDate';
import { isInteger } from '../../../validate/isInteger';
import { isLogger } from '../../../validate/isLogger';
import { isNonEmptyJson } from '../../../validate/isNonEmptyJson';
import { isNonEmptyString } from '../../../validate/isNonEmptyString';
import { isReadStream } from '../../../validate/isReadStream';

class FollowupMinerValidator {
  constructor() { };

  dataLegal(obj) {
    return this.isArrayOfJsons(obj);
  }

  dataStreamLegal(obj) {
    return this.isReadStream(obj);
  }

  dateLegal(obj) {
    return this.isDate(obj);
  }

  envLegal(obj) {
    return this.isNonEmptyJson(obj);
  }

  isArrayOfJsons(obj) {
    return isArrayOfJsons(obj);
  }

  isDate(obj) {
    return isDate(obj);
  }

  isInteger(obj) {
    return isInteger(obj);
  }
  
  isLogger(obj) {
    return isLogger(obj);
  }
  
  isNonEmptyJson(obj) {
    return isNonEmptyJson(obj);
  }

  isNonEmptyString(obj) {
    return isNonEmptyString(obj);
  }

  isReadStream(obj) {
    return isReadStream(obj);
  }

  loggerLegal(obj) {
    return this.isLogger(obj);
  }

  pageLegal(obj) {
    return this.isInteger(obj);
  }

  pageSizeLegal(obj) {
    if(this.isInteger(obj)) {
      return obj > 0;
    }
    return false;
  }

  titleLegal(obj) {
    return this.isNonEmptyString(obj);
  }

}

export { FollowupMinerValidator }