const fs = require('fs');
const htmlparser = require('htmlparser2');
const https = require('https');
const zlib = require('zlib');

import { Errors } from './errors';
import { FollowupMiner } from './miner';

class DuckDuckGoMiner extends FollowupMiner {
  
  constructor(args) {
    super(args);
    this._name = Object.getPrototypeOf(this).constructor.name;
  }
  
  async links(title, date, page, pageSize) {
    this.logger().info(this._name, 'executing the links() method');
    let debugMsg = 'links() arguments:';
    debugMsg += `\n\ttitle:\t${title},\n\tdate:\t${date},\n\tpage:\t${page},`;
    debugMsg += `\n\tpageSize:\t${pageSize}`;
    this.logger().debug(this._name, debugMsg);
    await super.links(title, date, page, pageSize);
    let dataStream = await this._rawData(title, date);
    this.logger().debug(this._name, 'links() method:\n\tdataStream parameter is ready');
    const data = await this._parseRawData(dataStream);
    this.logger().debug(this._name, 'links() method:\n\tdata parameter is ready');
    const result = await this._filterData(data, page, pageSize);
    debugMsg = `links() method:\n\tresult:\t{\n\t\t${result}\n}`;
    this.logger().debug(this._name, debugMsg);
    return new Promise((res, rej) => {
      res(result);
    });
  }

  async _filterData(data, page, pageSize) {
    this.logger().info(this._name, 'executing the _filterData() method');
    let debugMsg = '_filterData() arguments:'
    debugMsg += `\n\tdata:\t${data},\n\tpage:\t${page},\n\tpageSize:\t${pageSize}`;
    this.logger().debug(this._name, debugMsg);
    try {
      this._areFilterDataArgsLegal(data, page, pageSize);
    } catch(err) {
      return new Promise((res, rej) => {
        rej(err);
      });
    }
    let passElements = page * pageSize;
    let pageSizeCopy = pageSize;
    debugMsg = '_filterData() method:';
    debugMsg += `\n\tpassElements:\t${passElements},\n\tpageSizeCopy:\n\t${pageSizeCopy}`;
    this.logger().debug(this._name, debugMsg);
    let result = await this._fillInPage(data, pageSizeCopy, passElements);
    return new Promise((res, rej) => {
      res(result);
    });
  }
  
  async _fillInPage(data, pageSize, passElements) {
    this.logger().info(this._name, 'executing the _fillInPage() method');
    let debugMsg = '_fillInPage() arguments:'
    debugMsg += `\n\tdata:\t${data},\n\tpageSize:\t${pageSize},\n\tpassElements:\t${passElements}`;
    this.logger().debug(this._name, debugMsg);
    try {
      this._areFillInPageArgsLegal(data, pageSize, passElements);
    } catch(err) {
      return new Promise((res, rej) => {
        rej(err);
      });
    }
    let result = JSON.parse('{}');
    for(let k in data) {
      if(passElements === 0 && pageSize > 0) {
        result[k] = data[k];
        pageSize--;
      } else {
        passElements--;
      }
    }
    return new Promise((res, rej) => {
      return res(result);
    });
  }

  async _parseRawData(dataStream) {
    this.logger().info(this._name, 'executing the _parseRawData() method');
    if(!this._validator.dataStreamLegal(dataStream)) {
      return new Promise((res, rej) => {
        rej(new Error(Errors.DATA_STREAM_IS_ILLEGAL));
      });
    }
    const result = new Promise((res, rej) => {
      let followups = [];
      let title = '';
      let followupNode = false;
      const parser = new htmlparser.Parser({
        onopentag(node, attr) {
          if (node == 'h2' && attr && attr.class && attr.class === 'result__title') {
            followupNode = true;
            followups.push({});
          } else if(node == 'a' && followupNode && attr && attr.class == 'result__a') {
            followups[followups.length - 1]['link'] = attr.href;
          }
        },
        onclosetag(node) {
          if (node == 'h2' && followupNode) {
            followupNode = false;
            followups[followups.length - 1]['title'] = title.trim();
            title = '';
          }
        },
        ontext(t) {
          if (followupNode) {
            title = title+' '+t.trim();
          }
        }
      }, { decodeEntities: true });
      parser.on('error', (e) => {
        return rej(e);
      });
      dataStream.on('error', (e) => {
        return rej(e);
      });
      dataStream.on('data', (chunk) => {
        parser.write(chunk);
      });
      dataStream.on('end', () => {
        parser.end();
        return res(followups);
      });
    });
    return result;
  }

  async _rawData(title, date) {
    this.logger().info(this._name, 'executing the _rawData() method');
    let debugMsg = '_rawData() arguments:';
    debugMsg += `\n\ttitle:\t${title},\n\tdate:\t${date}`;
    this.logger().debug(this._name, debugMsg);
    try {
      this._areRawDataArgsLegal(title, date);
    } catch(err) {
      return new Promise((res, rej) => {
        rej(err);
      });
    }
    const result = new Promise((resolve, rej) => {
      let query = undefined;
      try {
        query = '/?q=' + this._titleToQueryParam(title) + '+' + this._dateToQueryParam(date);
      } catch(err) {
        return rej(err);
      }
      const options = {
        host: this.env().miner.duckduckgo.host,
        path: this.env().miner.duckduckgo.path + query,
        headers: {
          "Accept": this.env().miner.duckduckgo.accept,
          "Accept-Encoding": this.env().miner.duckduckgo.accept_encoding,
          "Accept-Language": this.env().miner.duckduckgo.accept_language,
          "Connection": this.env().miner.duckduckgo.connection,
          "TE": this.env().miner.duckduckgo.te,
          "Upgrade-Insecure-Requests": this.env().miner.duckduckgo.upgrade_insecure_requests,
          "User-Agent": this.env().miner.duckduckgo.agent
        }
      }
      const filePath = this.env().miner.duckduckgo.response_asset;
      https.get(options, (res) => {
        const out = fs.createWriteStream(filePath);
        const encoding = res.headers['content-encoding']
        switch(encoding) {
          case 'br':
            res.pipe(zlib.createBrotliDecompress()).pipe(out);
            break;
          case 'gzip':
            res.pipe(zlib.createGunzip()).pipe(out);
            break;
          case 'deflate':
            res.pipe(zlib.createInflate()).pipe(out);
            break;
          default:
            const errMsg = `content-encoding has unexpected value: ${encoding}`;
            return rej(errMsg);
          }
        res.on('error', (e) => {
          out.end(() => {
            return rej(e);
          });
        });
        out.on('close', () => {
          const input = fs.createReadStream(filePath);
          return resolve(input);
        });
      }).on('error', (e) => {
        return rej(e);
      });
    });
    return result;
  }

  _areFilterDataArgsLegal(data, page, pageSize) {
    if(!this._validator.dataLegal(data)) {
      throw new Error(Errors.DATA_IS_ILLEGAL);
    }
    if(!this._validator.pageLegal(page)) {
      throw new Error(Errors.PAGE_IS_ILLEGAL);
    }
    if(!this._validator.pageSizeLegal(pageSize)) {
      throw new Error(Errors.PAGE_SIZE_IS_ILLEGAL);
    }
  }

  _areFillInPageArgsLegal(data, pageSize, passElements) {
    if(!this._validator.dataLegal(data)) {
      throw new Error(Errors.DATA_IS_ILLEGAL);
    }
    if(!this._validator.pageSizeLegal(pageSize)) {
      throw new Error(Errors.PAGE_SIZE_IS_ILLEGAL);
    }
    if(!this._validator.isInteger(passElements)) {
      throw new Error(Errors.OBJECT_NOT_AN_INTEGER);
    }
  }

  _areRawDataArgsLegal(title, date) {
    if(!this._validator.titleLegal(title)) {
      throw new Error(Errors.OBJECT_NOT_A_TITLE);
    }
    if(!this._validator.dateLegal(date)) {
      throw new Error(Errors.OBJECT_NOT_A_DATE);
    }
  }

  _dateToQueryParam(date) {
    this.logger().info(this._name, 'executing the _dateToQueryParam() method');
    if(!this._validator.dateLegal(date)) {
      throw new Error(Errors.OBJECT_NOT_A_DATE);
    } else {
      return `${date.getDate()}+${date.getMonth() + 1}+${date.getFullYear()}`;
    }
  }

  _titleToQueryParam(title) {
    this.logger().info(this._name, 'executing the _titleToQueryParam() method');
    let result = undefined;
    if(this._validator.titleLegal(title)) {
      const lowerCaseTitle = title.toLowerCase();
      result = '';
      for (let i in lowerCaseTitle) {
        if(title[i] == ' ') {
          result += '+';
        } else {
          result += lowerCaseTitle[i];
        }
      }
      return result;
    } else {
      throw new Error(Errors.OBJECT_NOT_A_TITLE);
    }
  }

}

export { DuckDuckGoMiner }