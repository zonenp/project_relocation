const http = require('http');

import { EconomicCalendar } from './calendar';
import { Errors } from './errors';

class ForexFactoryCalendar extends EconomicCalendar {
  constructor(args) {
    super(args);
    this._maxEntries = 0;
    this._name = Object.getPrototypeOf(this).constructor.name;
  }
  
  async data(startDate, endDate, page, pageSize) {
    // The caller is responsible for taking care of the error
    this.logger().info(this._name, 'executing the data() method');
    let debugMsg = 'data() arguments:';
    debugMsg += `\n\tstartDate:\t${startDate},\n\tendDate:\t${endDate},`;
    debugMsg += `\n\tpage:\t${page},\n\tpageSize:\t${pageSize}`;
    this.logger().debug(this._name, debugMsg);
    await super.data(startDate, endDate, page, pageSize);
    const rawData = await this._rawWeeklyData(startDate);
    this.logger().debug(this._name, `data() method:\n\trawData parameter is ready`);
    const data = await this._parseRawData(rawData);
    this.logger().debug(this._name, `data() method:\n\tdata parameter is ready`);
    const result = await this._filterData(data, page, pageSize);
    debugMsg = `data() method:\n\tresult:\t{\n\t\t${result.maxEntries},`;
    debugMsg += `\n\t\t${result.entries}\n\t}`;
    this.logger().debug(this._name, debugMsg);
    return new Promise((resolve, reject) => {
      resolve(result);
    });
  }

  async _filterData(data, page, pageSize) {
    this.logger().info(this._name, 'executing the _filterData() method');
    let debugMsg = '_filterData() arguments:';
    debugMsg += `\n\tdata:\t${data},\n\tpage:\t${page},\n\tpageSize:\t${pageSize}`;
    this.logger().debug(this._name, debugMsg);
    if(!this._validator.isPage(page)) {
      return new Promise((resolve, reject) => { reject(new Error(Errors.PAGE_ILLEGAL)) });
    }
    if(!this._validator.isPageSize(pageSize)) {
      return new Promise((resolve, reject) => { reject(new Error(Errors.PAGE_SIZE_ILLEGAL)) });
    }
    if(!this._validator.isArrayOfJsons(data)) {
      return new Promise((resolve, reject) => {
        reject(new Error(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS))
      });
    }
    let elementsToPass = page * pageSize;
    let pageSizeCopy = pageSize;
    if(!this._validator.isInteger(elementsToPass)) {
      return new Promise((resolve, reject) => {
        reject(new Error(Errors.OBJECT_IS_NOT_AN_INTEGER));
      });
    }
    let result = await this._fillInPage(data, pageSizeCopy, elementsToPass);
    if(!this._validator.isArrayOfJsons(result.entries)) {
      result = await this._fillInPage(data, pageSize, 0);
    }
    return new Promise((resolve, reject) => {
      resolve(result)
    });
  }
  
  async _fillInPage(data, pageSize, elementsToPass) {
    this.logger().info(this._name, 'executing the _fillInPage() method');
    let debugMsg  = '_fillInPage() arguments:';
    debugMsg += `\n\tdata:\t${data},\n\tpageSize:\t${pageSize},\n\telementsToPass:\t${elementsToPass}`;
    this.logger().debug(this._name, debugMsg);
    if(!this._validator.isArrayOfJsons(data)) {
      return new Promise((res, rej) => {
        rej(new Error(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS));
      });
    }
    if(!this._validator.isPageSize(pageSize)) {
      return new Promise((resolve, reject) => { reject(new Error(Errors.PAGE_SIZE_ILLEGAL)); });
    }
    if(!this._validator.isInteger(elementsToPass)) {
      return new Promise((resolve, reject) => { reject(new Error(Errors.OBJECT_IS_NOT_AN_INTEGER)); });
    }
    const result =  new Promise((resolve, reject) => {
      let entries = JSON.parse('{}');
      for(let k in data) {
        if(elementsToPass === 0 && pageSize > 0) {
          entries[k] = data[k];
          pageSize--;
        } else {
          elementsToPass--;
        }
      }
      resolve({
        maxEntries: this._maxEntries,
        entries: entries
      });
    });
    return result;
  }
  
  async _parseRawData(rawData) {
    this.logger().info(this._name, 'executing the _parseRawData() method');
    let debugMsg = `_parseRawData() arguments:\n\trawData:\t${rawData}`;
    this.logger().debug(this._name, debugMsg);
    const result = new Promise((resolve, reject) => {
      if(rawData) {
        try {
          const result = JSON.parse(rawData);
          this._setMaxEntries(Object.keys(result).length);
          return resolve(result);
        } catch(e) {
          return reject(new Error(Errors.OBJECT_IS_NOT_A_RAW_JSON));
        }
      } else {
        return reject(new Error(Errors.OBJECT_IS_NOT_A_RAW_JSON));
      }
    });
    return result;
  }

  async _rawWeeklyData(startDate) {
    // startDate is validated at the data() method.
    this.logger().info(this._name, 'executing the _rawWeeklyData() method');
    let debugMsg = `_rawWeeklyData() arguments:\n\tstartDate:\t${startDate}`;
    this.logger().debug(this._name, debugMsg);
    let query = undefined;
    try {
      query = this._dateToQueryParam(startDate);
    } catch(err) {
      return new Promise((res, rej) => {
        rej(err);
      });
    }
    const options = {
      host: this._env.economic_calendar.forex_factory.host,
      path: this._env.economic_calendar.forex_factory.path+query
    }
    const result = new Promise((resolve, reject) => {
      let result = '';
      http.get(options, (res) => {
        res.on('data', (chunk) => {
          result += chunk;
        });
        res.on('end', () => {
          return resolve(result);
        });
      }).on('error', (e) => {
        return reject(e);
      });
    });
    return result;
  }

  _dateToQueryParam(date) {
    // For some reason the .getDate() and .getFullYear() take more than 1 sec to execute.
    this.logger().info(this._name, 'executing the _dateToQueryParam() method');
    let debugMsg = `_dateToQueryParam() arguments:\n\tdate:\t${date}`;
    this.logger().debug(this._name, debugMsg);
    let result = undefined;
    if(this._validator.isDate(date)) {
      result = `${date.getDate()}.${date.getFullYear()}`;
    } else {
      throw new Error(Errors.OBJECT_IS_NOT_A_DATE);
    }
    return result;
  }

  _setMaxEntries(count) {
    if(!this._validator.isInteger(count)) {
      throw new Error(Errors.OBJECT_IS_NOT_AN_INTEGER);
    }
    this._maxEntries = count;
  }
}

export { ForexFactoryCalendar }