const Errors = Object.freeze({
  ARGS_TO_API_ARE_ILLEGAL: 'Args to Economic Calendar API are illegal.',
  DATE_RANGE_ILLEGAL: 'Date range is illegal. Expected start date <= end date',
  ECONOMIC_CALENDAR_SOURCE_ILLEGAL: 'Economic Calendar Source is illegal. Expected an enum __EconomicCalendarSource__',
  ENV_ILLEGAL: 'Env is illegal. Expected a non empty __JSON__',
  FF_CALENDAR_INIT_FAILED: 'Failed to initialize the __ForeFactoryCalendar__ object',
  LOGGER_ILLEGAL: 'Logger is illegal. Expected a __Logger__',
  OBJECT_IS_NOT_AN_ARRAY_OF_JSONS: 'Object is illegal. Expected an __Array<JSON>__',
  OBJECT_IS_NOT_A_DATE: 'Date is illegal. Expected a __Date__',
  OBJECT_IS_NOT_AN_INTEGER: 'Expected an __int__',
  OBJECT_IS_NOT_A_RAW_JSON: 'Raw data is illegal. Expected a __stringified JSON__',
  PAGE_ILLEGAL: 'Page is illegal. Expected an __int__ >= 0',
  PAGE_SIZE_ILLEGAL: 'Page size is illegal. Expected an __int__ > 0'
});

export { Errors }