import { isArrayOfJsons } from '../../../validate/isArrayOfJsons';
import { isDate } from '../../../validate/isDate';
import { isInteger } from '../../../validate/isInteger';
import { isLogger } from '../../../validate/isLogger';
import { isNonEmptyJson } from '../../../validate/isNonEmptyJson';

class EconomicCalendarValidator {

  constructor() { }

  isArrayOfJsons(obj) {
    return isArrayOfJsons(obj);
  }

  isDate(obj) {
    return isDate(obj);
  }

  isDateRangeLegal(startDate, endDate) {
    return !(startDate > endDate);
  }
  
  isEnv(obj) {
    return this.isNonEmptyJson(obj);
  }

  isInteger(obj) {
    return isInteger(obj);
  }

  isLogger(obj) {
    return isLogger(obj);
  }

  isNonEmptyJson(obj) {
    return isNonEmptyJson(obj);
  }

  isPage(value) {
    return this.isInteger(value);
  }
  
  isPageSize(value) {
    if(this.isInteger(value)) {
      return value >= 1;
    }
  }
}

export { EconomicCalendarValidator }