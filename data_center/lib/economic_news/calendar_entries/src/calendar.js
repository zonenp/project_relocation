import { DaysOfWeek } from './days_of_week';
import { Errors } from './errors';
import { EconomicCalendarValidator } from './validator';

class EconomicCalendar {
  // This is an abstract class.

  constructor(args) {
    this._validator = new EconomicCalendarValidator();
    this._setEnv(args.env);
    this._setLogger(args.logger);
    this._name = Object.getPrototypeOf(this).constructor.name;
  }

  async data(startDate, endDate, page, pageSize) {
    // The caller is responsible for taking care of the errors
    this.logger().info(this._name, 'executing the data() method');
    let debugMsg = 'data() arguments:';
    debugMsg += `\n\tstartDate:\t${startDate},\n\tendDate:\t${endDate},`
    debugMsg += `\n\tpage:\t${page},\n\tpageSize:\t${pageSize}`;
    this.logger().debug(this._name, debugMsg);
    try {
      this._areArgsDataLegal(startDate, endDate, page, pageSize);
    } catch(err) {
      return new Promise((resolve, reject) => {
        reject(err);
      });
    }
    // descendants implementation
  }

  env() {
    return this._env;
  }

  logger() {
    return this._logger;
  }

  validator() {
    return this._validator;
  }

  _areArgsDataForCurrentWeekLegal(page, pageSize) {
    if(!this.validator().isPage(page)) {
      throw new Error(Errors.PAGE_ILLEGAL);
    }
    if(!this.validator().isPageSize(pageSize)) {
      throw new Error(Errors.PAGE_SIZE_ILLEGAL);
    }
  }

  _areArgsDataLegal(startDate, endDate, page, pageSize) {
    if(!this.validator().isDate(startDate) || !this.validator().isDate(endDate)) {
      throw new Error(Errors.OBJECT_IS_NOT_A_DATE);
    }
    if(!this.validator().isDateRangeLegal(startDate, endDate)) {
      throw new Error(Errors.DATE_RANGE_ILLEGAL);
    }
    if(!this.validator().isPage(page)) {
      throw new Error(Errors.PAGE_ILLEGAL);
    }
    if(!this.validator().isPageSize(pageSize)) {
      throw new Error(Errors.PAGE_SIZE_ILLEGAL);
    }
  }

  _setEnv(obj) {
    if (this._validator.isEnv(obj)) {
      this._env = obj;
    } else {
      throw new Error(Errors.ENV_ILLEGAL);
    }
  }

  _setLogger(obj) {
    if(this.validator().isLogger(obj)) {
      this._logger = obj;
    } else {
      throw new Error(Errors.LOGGER_ILLEGAL);
    }
  }

}

export { EconomicCalendar }