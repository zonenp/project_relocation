import { EconomicCalendarSource } from './src/source';
import { Errors } from './src/errors';
import { ForexFactoryCalendar } from './src/forex_factory';

async function economicCalendarEntries(args) {
  if(args && args.params && args.params.source) {
    switch(args.params.source) {
      case EconomicCalendarSource.FOREX_FACTORY:
        let ffCalendar = undefined;
        try {
          ffCalendar = new ForexFactoryCalendar({
            env: args.env,
            logger: args.logger
          }); 
        } catch(err) {
          return new Promise((res, rej) => {
            rej(new Error(Errors.FF_CALENDAR_INIT_FAILED));
          });
        }
        let startDate = undefined;
        let endDate = undefined;
        try {
          if(args.params.start_date && args.params.end_date) {
            startDate = new Date(args.params.start_date);
            endDate = new Date(args.params.end_date);
          } else {
            throw new Error();
          }
        } catch(err) {
          return new Promise((res, rej) => {
            rej(new Error(Errors.OBJECT_IS_NOT_A_DATE));
          });
        }
        return ffCalendar.data(
          startDate,
          endDate,
          parseInt(args.params.page),
          parseInt(args.params.page_size)
        );
      default:
        return new Promise((res, rej) => {
          rej(new Error(Errors.ECONOMIC_CALENDAR_SOURCE_ILLEGAL));
        });
    }
  } else {
    return new Promise((res, rej) => {
      rej(new Error(Errors.ARGS_TO_API_ARE_ILLEGAL));
    })
  }
}

export { economicCalendarEntries }