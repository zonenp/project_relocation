const expect = require('chai').expect;

import { config } from '../../../../config/config.dev';
import { EconomicCalendar } from '../src/calendar';
import { Errors } from '../src/errors';
import { Logger } from '../../../logger/logger';
import { LogStream } from '../../../logger/src/stream';

describe('EconomicCalendar', () => {
  describe('env argument', function() {
    before(function() {
      this.logger = new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE });
    });
    
    it('should throw an error when the env is missing', () => {
      expect(() => { new EconomicCalendar({
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is null', () => {
      expect(() => { new EconomicCalendar({
        env: null,
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is undefined', () => {
      expect(() => { new EconomicCalendar({
        env: undefined,
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is of illegal type', () => {
      expect(() => { new EconomicCalendar({
        env: '{"key": "value"}',
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });

    it('should throw an error when the env is empty', () => {
      expect(() => { new EconomicCalendar({
        env: {},
        logger: this.logger
      }) }).to.throw(Errors.ENV_ILLEGAL);
    });
  });

  describe('logger argument', () => {
    it('should throw an error when the logger is missing', () => {
      expect(() => { new EconomicCalendar({
        env: config
      }) }).to.throw(Errors.LOGGER_ILLEGAL);
    });

    it('should throw an error when the logger is null', () => {
      expect(() => { new EconomicCalendar({
        env: config,
        logger: null
      }) }).to.throw(Errors.LOGGER_ILLEGAL);
    });

    it('should throw an error when the logger is undefined', () => {
      expect(() => { new EconomicCalendar({
        env: config,
        logger: undefined
      }) }).to.throw(Errors.LOGGER_ILLEGAL);
    });
  });
  
  it('should create an object.', () => {
    const calendar = new EconomicCalendar({
      env: config,
      logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
    });
    expect(calendar.constructor.name).to.equal('EconomicCalendar');
  });
  
  describe('data method', () => {
    describe('startDate argument', function() {
      before(function() {
        this.calendar = new EconomicCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
      });
      
      it('should reject with an error when the start date is null.', function(done) {
        this.calendar.data(null, new Date(), 0, 1)
        .catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      });
      
      it('should reject with an error when start date is undefined', function(done) {
        this.calendar.data(undefined, new Date(), 0, 1).catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      });
      
      it('should reject with an error when start date is not a Date', function(done) {
        this.calendar.data('2019.03.20', new Date(), 0, 1).catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      });
    });

    describe('endDate argument', function() {
      before(function() {
        this.calendar = new EconomicCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
      });
      
      it('should reject with an error when end date is null', function(done) {
        this.calendar.data(new Date(), null, 0, 1).catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      });
      
      it('should reject with an error when end date is undefined', function(done) {
        this.calendar.data(new Date(), undefined, 0, 1).catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      });
      
      it('should reject with an error when end date is not a Date', function(done) {
        this.calendar.data(new Date(), '2019.03.20', 0, 1).catch((err) => {
          expect(err.message).to.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      });
    });

    describe('date range', function() {
      before(function() {
        this.calendar = new EconomicCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
      });

      it('should reject with an error when the start date > end date', function(done) {
        this.calendar.data(new Date(), new Date('2019-09-10'), 0, 1).catch((err) => {
          expect(err.message).to.equal(Errors.DATE_RANGE_ILLEGAL);
          done();
        });
      });
    });
    
    describe('page argument', function() {
      before(function() {
        this.calendar = new EconomicCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
      });
      
      it('should reject with an error when the page is null', function(done) {
        this.calendar.data(new Date(), new Date(), null, 1).catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_ILLEGAL);
          done();
        });
      });
      
      it('should reject with an error when the page is undefined', function(done) {
        this.calendar.data(
          new Date(), new Date(), undefined, 1
          ).catch((err) => {
            expect(err.message).to.be.equal(Errors.PAGE_ILLEGAL);
            done();
          }
        )
      });
      
      it('should reject with an error when the page has illegal value', function(done) {
        this.calendar.data(new Date(), new Date(), -1, 1).catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_ILLEGAL);
          done();
        });
      });
    });
    
    describe('pageSize argument', function() {
      before(function() {
        this.calendar = new EconomicCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        });
      });
      
      it('should reject with an error when the pageSize is null', function(done) {
        this.calendar.data(new Date(), new Date(), 1, null).catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_SIZE_ILLEGAL);
          done();
        });
      });
      
      it('should reject with an error when the pageSize is undefined', function(done) {
        this.calendar.data(new Date(), new Date(), 1, undefined).catch((err) => {
            expect(err.message).to.be.equal(Errors.PAGE_SIZE_ILLEGAL);
            done();
          }
        )
      });
      
      it('should reject with an error when the pageSize has illegal value', function(done) {
        this.calendar.data(new Date(), new Date(), 1, -1).catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_SIZE_ILLEGAL);
          done();
        });
      });
    });
  });
  
});