const expect = require('chai').expect;

import { config } from '../../../../config/config.dev';
import { economicCalendarEntries } from '../api';
import { EconomicCalendarSource } from '../src/source';
import { Errors } from '../src/errors';
import { Logger } from '../../../logger/logger';
import { LogStream } from '../../../logger/src/stream';

describe('Economic Calendar API', () => {
  describe('economicCalendarEntries()', () => {
    describe('args argument', () => {
      it('should reject with an error when the args is null', (done) => {
        economicCalendarEntries(null).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the args is undefined', (done) => {
        economicCalendarEntries(undefined).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the args has no keys', (done) => {
        economicCalendarEntries(23).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });
    });

    describe('args.params argument', () => {
      it('should reject with an error when the params is non-existant', (done) => {
        economicCalendarEntries({one: 2}).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the params is null', (done) => {
        economicCalendarEntries({params: null}).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the params is undefined', (done) => {
        economicCalendarEntries({params: undefined}).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the params has no keys', (done) => {
        economicCalendarEntries({params: 24}).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });
    });

    describe('args.params.source argument', () => {
      it('should reject with an error when the source is non-existant', (done) => {
        economicCalendarEntries({
          params: {one: 23}
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the source is null', (done) => {
        economicCalendarEntries({
          params: {source: null}
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the source is null', (done) => {
        economicCalendarEntries({
          params: {source: undefined}
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.ARGS_TO_API_ARE_ILLEGAL);
          done();
        });
      });

      it('should reject with an error when the source has illegal value', (done) => {
        economicCalendarEntries({
          params: {source: 'nonexistant calendar'}
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.ECONOMIC_CALENDAR_SOURCE_ILLEGAL);
          done();
        });
      });
    });

    describe('forex factory calendar initialization', () => {
      before(function() {
        this.logger = new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE });
      });

      it('should reject with an error when env argument is not legal', function(done) {
        economicCalendarEntries({
          logger: this.logger,
          params: {source: EconomicCalendarSource.FOREX_FACTORY}
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.FF_CALENDAR_INIT_FAILED);
          done();
        });
      });

      it('should reject with an error when logger argument is not legal', function(done) {
        economicCalendarEntries({
          env: config,
          params: {source: EconomicCalendarSource.FOREX_FACTORY}
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.FF_CALENDAR_INIT_FAILED);
          done();
        });
      });
    });

    describe('params argument', () => {
      before(function() {
        this.logger = new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE });
      });

      it('should reject with an error when the start date is null', function(done) {
        economicCalendarEntries({
          env: config,
          logger: this.logger,
          params: {
            source: EconomicCalendarSource.FOREX_FACTORY,
            start_date: null,
            end_date: '2020-02-21',
            page: '0',
            page_size: '10'
          }
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      })

      it('should reject with an error when the start date is undefined', function(done) {
        economicCalendarEntries({
          env: config,
          logger: this.logger,
          params: {
            source: EconomicCalendarSource.FOREX_FACTORY,
            start_date: undefined,
            end_date: '2020-02-21',
            page: '0',
            page_size: '10'
          }
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      })

      it('should reject with an error when the end date is null', function(done) {
        economicCalendarEntries({
          env: config,
          logger: this.logger,
          params: {
            source: EconomicCalendarSource.FOREX_FACTORY,
            start_date: '2020-02-21',
            end_date: null,
            page: '0',
            page_size: '10'
          }
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      })

      it('should reject with an error when the end date is undefined', function(done) {
        economicCalendarEntries({
          env: config,
          logger: this.logger,
          params: {
            source: EconomicCalendarSource.FOREX_FACTORY,
            start_date: '2020-02-21',
            end_date: undefined,
            page: '0',
            page_size: '10'
          }
        }).catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_DATE);
          done();
        });
      })
    });
  });
});