const expect = require('chai').expect;

import { config } from '../../../../config/config.dev';
import { Errors } from '../src/errors';
import { ForexFactoryCalendar } from '../src/forex_factory';
import { isArrayOfJsons } from '../../../validate/isArrayOfJsons';
import { Logger } from '../../../logger/logger';
import { LogStream } from '../../../logger/src/stream';

describe('ForexFactoryCalendar', () => {
  describe('data method', () => {
    before(function() {
      this.forexFactory = new ForexFactoryCalendar({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.start = new Date();
      this.end = new Date();
      this.end.setDate(this.end.getDate() + 1);
    });
    
    it('should retrieve the first page of data', function(done) {
      this.forexFactory.data(this.start, this.end, 0, 1)
        .then((data) => {
          expect(data.maxEntries).to.be.greaterThan(0);
          expect(data.entries.length == 1)
          done();
        });
    }).timeout(1000);
    
    it('should retrieve the second page of data', function(done) {
      this.forexFactory.data(this.start, this.end, 2, 2)
        .then((data) => {
          expect(data.maxEntries).to.be.greaterThan(0);
          expect(data.length == 2)
          done();
        });
    });
  });
  
  describe('_fillInPage method', () => {
    before(function() {
      this.forexFactory = new ForexFactoryCalendar({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE }) 
      });
      this.legalPageSize = 1;
      this.legalElementsToPass = 0;
      this.emptyData = JSON.parse('[]');
      this.data = JSON.parse('[{"1": 2}, {"2": 3}, {"3": 4}, {"4": 5}]');
    });
    
    it('should reject with an error when data is null', function(done) {
      this.forexFactory._fillInPage(null, this.legalPageSize, this.legalElementsToPass)
      .catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
        done();
      });
    });
    
    it('should reject with an error when data is undefined', function(done) {
      this.forexFactory._fillInPage(undefined, this.legalPageSize, this.legalElementsToPass)
      .catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
        done();
      });
    });
    
    it('should reject with an error when data is not an Array', function(done) {
      this.forexFactory._fillInPage('[{"1": 2}]', this.legalPageSize, this.legalElementsToPass)
      .catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
        done();
      });
    });
    
    it('should reject with an error when data is enumerable, yet not an Array', function(done) {
      class A { constructor() { this.a = 'b'; } }
      this.forexFactory._fillInPage(new A(), this.legalPageSize, this.legalElementsToPass)
      .catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
        done();
      });
    });
    
    it('should reject with an error when data is an empty Array', function(done) {
      this.forexFactory._fillInPage(this.emptyData, this.legalPageSize, this.legalElementsToPass)
      .catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
        done();
      });
    });
    
    it('should reject with an error when the page size is of illegal type', function(done) {
      this.forexFactory._fillInPage(this.data, '1', this.legalPageSize * 1)
      .catch((error) => {
        expect(error.message).to.be.equal(Errors.PAGE_SIZE_ILLEGAL);
        done();
      });
    });
    
    it('should reject with an error when the page size has an illegal value', function(done) {
      this.forexFactory._fillInPage(this.data, 0, this.legalPageSize * 1)
      .catch((error) => {
        expect(error.message).to.be.equal(Errors.PAGE_SIZE_ILLEGAL);
        done();
      });
    });
    
    it('should reject with an error when the elements to pass is of illegal type', function(done) {
      this.forexFactory._fillInPage(this.data, this.legalPageSize, '1')
      .catch((error) => {
        expect(error.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_INTEGER);
        done();
      });
    });
    
    it('should reject with an error when the elements to pass has an illegal value', function(done) {
      this.forexFactory._fillInPage(this.data, this.legalPageSize, -1)
      .catch((error) => {
        expect(error.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_INTEGER);
        done();
      });
    });
    
    it('should return the first page', function(done) {
      this.forexFactory._fillInPage(this.data, this.legalPageSize, this.legalElementsToPass)
      .then((page) => {
        let count = 0;
        for(let k in page.entries) {
          count++;
          expect(page.entries[k]).to.be.equal(this.data[k]);
        }
        expect(count).to.be.equal(this.legalPageSize);
        done();
      });
    });
    
    it('should skip a page and return the second page', function(done) {
      this.forexFactory._fillInPage(this.data, this.legalPageSize, this.legalPageSize * 1)
      .then((page) => {
        let count = 0;
        for(let k in page.entries) {
          count++;
          expect(page.entries[k]).to.be.equal(this.data[k]);
        }
        expect(count).to.be.equal(this.legalPageSize);
        done();
      });
    });
  });
  
  describe('_filterData method', () => {
    before(function() {
      this.forexFactory = new ForexFactoryCalendar({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.legalPage = 0;
      this.legalPageSize = 1;
      this.legalElementsToPass = 0;
      this.emptyData = JSON.parse('[]');
      this.legalData = JSON.parse('[{"1": 2}, {"2": 3}, {"3": 4}, {"4": 5}]');
    });
    
    it('should reject with an error when data is null', function(done) {
      this.forexFactory._filterData(null, this.legalPage, this.legalPageSize)
       .catch((err) => {
         expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
         done();
       });
    });
    
    it('should reject with an error when data is undefined', function(done) {
      this.forexFactory._filterData(undefined, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
          done();
        });
    });
    
    it('should reject with an error when data is not an Array object', function(done) {
      this.forexFactory._filterData('[{"1":2}]', this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
          done();
        });
    });
    
    it('should reject with an error when data is enumerable, yet not an Array', function(done) {
      class A {
        constructor() {
          this.a = 2;
          this.b = 2
        }
      }
      this.forexFactory._filterData(new A(), this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
          done();
        });
    });
    
    it('should reject with an error when data is an empty Array', function(done) {
      this.forexFactory._filterData(this.emptyData, this.legalPage, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_AN_ARRAY_OF_JSONS);
          done();
        });
    });
    
    it('should reject with an error when the page is of illegal type', function(done) {
      this.forexFactory._filterData(this.legalData, '1', this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_ILLEGAL);
          done();
        });
    });
    
    it('should reject with an error when the page has an illegal value', function(done) {
      this.forexFactory._filterData(this.legalData, -1, this.legalPageSize)
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_ILLEGAL);
          done();
        });
    });
    
    it('should reject with an error when the page size is not legal', function(done) {
      this.forexFactory._filterData(this.legalData, this.legalPage, '1')
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_SIZE_ILLEGAL);
          done();
        });
    });
    
    it('should reject with an error when the page size has an illegal value', function(done) {
      this.forexFactory._filterData(this.legalData, this.legalPage, -1)
        .catch((err) => {
          expect(err.message).to.be.equal(Errors.PAGE_SIZE_ILLEGAL);
          done();
        });
    });
    
    it('should return the first page', function(done) {
      this.forexFactory._filterData(this.legalData, 0, 2)
        .then((page) => {
          let count = 0;
          for(let k in page.entries) {
            expect(this.legalData[k]).to.be.equal(page.entries[k]);
            count++;
          }
          expect(count).to.be.equal(2);
          done();
        });
    });
    
    it('should return the second page', function(done) {
      this.forexFactory._filterData(this.legalData, 1, 3)
        .then((page) => {
          let count = 0;
          for(let k in page.entries) {
            expect(this.legalData[k]).to.be.equal(page.entries[k]);
            count++;
          }
          expect(count).to.be.equal(1);
          done();
        });
    });
    
    it('should return the first page when the end of data is reached', function(done) {
      this.forexFactory._filterData(this.legalData, 3, 2)
        .then((page) => {
          let count = 0;
          for(let k in page.entries) {
            expect(this.legalData[k]).to.be.equal(page.entries[k]);
            count++;
          }
          expect(count).to.be.equal(2);
          done();
        });
    });
  });
  
  describe('_parseRawData()', () => {
    before(function() {
      this.forexFactory = new ForexFactoryCalendar({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.legalRawData = '[{"1":2, "2": 3}]';
      this.illegalFormatRawData = '{"1:2]';
    });
    
    it('should reject with an error when rawData is null', function(done) {
      this.forexFactory._parseRawData(null).catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_RAW_JSON);
        done();
      });
    });
    
    it('should reject with an error when rawData is undefined', function(done) {
      this.forexFactory._parseRawData(undefined).catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_RAW_JSON);
        done();
      });
    });
    
    it('should reject with an error when rawData is of illegal type', function(done) {
      this.forexFactory._parseRawData(["1", 2]).catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_RAW_JSON);
        done();
      });
    });
    
    it('should reject with an error when rawData is contains wrong format', function(done) {
      this.forexFactory._parseRawData(this.illegalFormatRawData).catch((err) => {
        expect(err.message).to.be.equal(Errors.OBJECT_IS_NOT_A_RAW_JSON);
        done();
      });
    });
    
    it('should resolve with an Array', function(done) {
      this.forexFactory._parseRawData(this.legalRawData).then((data) => {
        expect(isArrayOfJsons(data)).to.be.true;
        done();
      });
    });
  });
  
  describe('_dateToQueryParam()', () => {
    it('should throw an error when the date is null', () => {
      expect( () => {
        new ForexFactoryCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        })._dateToQueryParam(null);
      }).to.throw(Errors.OBJECT_IS_NOT_A_DATE);
    });
    
    it('should throw an error when the date is undefined', () => {
      expect(() => {
        new ForexFactoryCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        })._dateToQueryParam(null);
      }).to.throw(Errors.OBJECT_IS_NOT_A_DATE);
    });
    
    it('should throw an error when the date is of the illegal type', () => {
      expect(() => {
        new ForexFactoryCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        })._dateToQueryParam('Thu Jul 25 2019 19:05:55 GMT+0300 (Israel Daylight Time)');
      }).to.throw(Errors.OBJECT_IS_NOT_A_DATE);
    });
    
    it('should return a date as a query for the forex factory', () => {
      expect(new ForexFactoryCalendar({
          env: config,
          logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
        })._dateToQueryParam(new Date('2019-07-25 19:15:00'))
      ).to.be.equal('25.2019');
    })
  });
  
  describe('_rawWeeklyData()', () => {
    before(function() {
      this.forexFactory = new ForexFactoryCalendar({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
      this.today = new Date();
    });
    
    it('should resolve with a raw weekly data from forex factory api', function(done) {
      this.forexFactory._rawWeeklyData(this.today).then((rawWeeklyData) => {
        expect(rawWeeklyData.length).to.be.above(1);
        expect(() => {
          JSON.parse(rawWeeklyData);
        }).to.not.throw();
        done();
      });
    }).timeout(1000);
  });

  describe('_setMaxEntries', () => {
    before(function() {
      this.forexFactory = new ForexFactoryCalendar({
        env: config,
        logger: new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE })
      });
    });

    it('should throw an error when the count is null', function() {
      expect(() => {
        this.forexFactory._setMaxEntries(null)
      }).to.throw(Errors.OBJECT_IS_NOT_AN_INTEGER);
    });

    it('should throw an error when the count is undefined', function() {
      expect(() => {
        this.forexFactory._setMaxEntries(undefined)
      }).to.throw(Errors.OBJECT_IS_NOT_AN_INTEGER);
    });

    it('should throw an error when the count is negative', function() {
      expect(() => {
        this.forexFactory._setMaxEntries(-1)
      }).to.throw(Errors.OBJECT_IS_NOT_AN_INTEGER);
    });

    it('should update the max entries when the count is legal', function() {
      this.forexFactory._setMaxEntries(5);
      expect(this.forexFactory._maxEntries).to.equal(5);
    });
  });
});