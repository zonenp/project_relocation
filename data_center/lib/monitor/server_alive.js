
class ServerAlive {
  constructor(args) {
    this._logger = args.logger;
    this._name = Object.getPrototypeOf(this).constructor.name;
  }

  pong() {
    this._logger.debug(this._name, 'executing the pong() method');
    return 'Alive';
  }
}

export { ServerAlive }