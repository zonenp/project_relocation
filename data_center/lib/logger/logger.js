const winston = require('winston');
require('winston-daily-rotate-file');

import { LoggerValidator } from './src/validator';
import { Errors } from './src/errors';
import { LogStream } from './src/stream';

class Logger {
  
  constructor(args) {
    this._validator = new LoggerValidator();
    this._setEnv(args.env);
    this._setDatePattern();
    this._setFormat();
    this._setLevel(args.level);
    this._setStream(args.logStream);
    this._setLogger();
  }

  debug(label, msg) {
    if(this._level == 'debug') {
      if (!this._validator.labelLegal(label)) {
        throw new Error(Errors.LABEL_ILLEGAL);
      } else if (!this._validator.messageLegal(msg)) {
        throw new Error((Errors.MSG_ILLEGAL));
      } else {
        this._logger.log({ level: 'debug', message: msg, label: label });
      }
    }
  }

  error(label, msg) {
    if (!this._validator.labelLegal(label)) {
      throw new Error(Errors.LABEL_ILLEGAL);
    } else if (!this._validator.messageLegal(msg)) {
      throw new Error((Errors.MSG_ILLEGAL));
    } else {
      this._logger.log({ level: 'error', message: msg, label: label });
    }
  }

  info(label, msg) {
    if (!this._validator.labelLegal(label)) {
      throw new Error(Errors.LABEL_ILLEGAL);
    } else if (!this._validator.messageLegal(msg)) {
      throw new Error((Errors.MSG_ILLEGAL));
    } else {
      this._logger.log({ level: 'info', message: msg, label: label });
    }
  }

  _log(level, label, msg) {
    if(!this._validator.levelLegal(level)) {
      throw new Error(Errors.LEVEL_ILLEGAL);
    } else if (!this._validator.labelLegal(label)) {
      throw new Error(Errors.LABEL_ILLEGAL);
    } else if (!this._validator.messageLegal(msg)) {
      throw new Error((Errors.MSG_ILLEGAL));
    } else {
      this._logger.log({ level: level, message: msg, label: label });
    }
  }

  _setDatePattern() {
    this._datePattern = this._env.log.date_pattern
  }

  _setEnv(env) {
    if(this._validator.envLegal(env)) {
      this._env = env;
    } else {
      throw new Error(Errors.ENV_ILLEGAL);
    }
  }

  _setFormat() {
    try {
      this._format = winston.format.printf(({ level, message, label, timestamp }) => {
        return `${timestamp} - ${level} - [${label}] - ${message}`;
      });
    } catch(err) {
      throw new Error(`${Errors.GENERAL_ERROR} - ${err.message}`);
    }
  }

  _setLevel(level) {
    if(this._validator.levelLegal(level)) {
      this._level = level;
    } else {
      throw new Error(Errors.LEVEL_ILLEGAL);
    }
  }

  _setStream(stream) {
    if(this._validator.streamLegal(stream)) {
      this._stream = stream;
    } else {
      throw new Error(Errors.STREAM_ILLEGAL);
    }
  }

  _setLogger() {
    switch(this._stream) {
      case(LogStream.CONSOLE):
        this._logger = winston.createLogger({
          format: winston.format.combine(
            winston.format.timestamp(),
            this._format
          ),
          transports: [
            new winston.transports.Console({
              level: this._level
            })
          ]
        });
        break;
      case(LogStream.FILE):
        let filename = '';
        if(this._level == 'debug') {
          filename = this._env.log.debug;
        } else {
          filename = this._env.log.info;
        }
        this._logger = winston.createLogger({
          format: winston.format.combine(
            winston.format.timestamp(),
            this._format
          ),
          transports: [
            new winston.transports.DailyRotateFile({
              datePattern: this._datePattern,
              level: this._level,
              filename: filename,
              maxFiles: this._env.log.max_files,
              maxSize: this._env.log.max_size,
              zippedArchive: this._env.log.zip
            })
          ]
        });
        break;
      default:
        throw new Error(Errors.STREAM_ILLEGAL);
    }
  }
}

export { Logger }