const LogStream = Object.freeze({
  CONSOLE: 'console',
  FILE: 'file'
});

export { LogStream }