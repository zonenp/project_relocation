const winston = require('winston');

import { isNonEmptyJson } from '../../validate/isNonEmptyJson';
import { isNonEmptyString } from '../../validate/isNonEmptyString';
import { LogStream } from './stream';

class LoggerValidator {

  envLegal(obj) {
    return this.isNonEmptyJson(obj);
  }

  isNonEmptyJson(obj) {
    return isNonEmptyJson(obj);
  }

  isNonEmptyString(obj) {
    return isNonEmptyString(obj);
  }

  labelLegal(obj) {
    return this.isNonEmptyString(obj);
  }

  levelLegal(obj) {
    if(this.isNonEmptyString(obj)) {
      return !(winston.config.syslog.levels[obj.toLowerCase()] == undefined)
    } else {
      return false;
    }
  }

  messageLegal(obj) {
    return this.isNonEmptyString(obj);
  }

  streamLegal(obj) {
    if(this.isNonEmptyString(obj)) {
      return !(LogStream[obj.toUpperCase()] == undefined);
    } else {
      return false;
    }
  }
  
}

export { LoggerValidator }