const Errors = Object.freeze({
  ENV_ILLEGAL: 'Env is illegal. Expected a non empty __JSON__',
  GENERAL_ERROR: 'Failed to setup the logger',
  STREAM_ILLEGAL: 'Logger stream is illegal. Expected a __LogStream__',
  MSG_ILLEGAL: 'Log message is illegal. Expected a non empty __string__',
  LEVEL_ILLEGAL: 'Log level is illegal. Expected a __string__ with the right value',
  LABEL_ILLEGAL: 'Label for log is illegal. Expected a non empty __string__'
});

export { Errors }