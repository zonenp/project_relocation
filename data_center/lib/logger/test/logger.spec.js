const expect = require('chai').expect;

import { config } from '../../../config/config.dev';
import { Logger } from '../logger';
import { LogStream } from '../src/stream';
import { Errors } from '../src/errors';

describe('Logger', () => {
  it('should create an object', () => {
    new Logger({ env: config, level: 'info', logStream: LogStream.CONSOLE });
    new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE });
    new Logger({ env: config, level: 'info', logStream: LogStream.FILE });
    new Logger({ env: config, level: 'debug', logStream: LogStream.FILE });
  });
  
  it('should log to the console', () => {
    const logger = new Logger({ env: config, level: 'debug', logStream: LogStream.CONSOLE });
    logger.info('Test', 'info test');
    logger.debug('Test', 'debug test');
    logger.error('Test', 'error test');
  });

  it('should log to the info log files', () => {
    const logger = new Logger({ env: config, level: 'info', logStream: LogStream.FILE });
    logger.info('Test', 'info test');
    logger.error('Test', 'error test');
  });

  it('should log to the debug log files', () => {
    const logger = new Logger({ env: config, level: 'debug', logStream: LogStream.FILE });
    logger.debug('Test Different', 'error test');
  });

  describe('env argument', () => {
    it('should throw an error when the env is missing', () => {
      expect(() => { new Logger({ level: 'debug', logStream: LogStream.FILE }) })
        .to.throw(Errors.ENV_ILLEGAL);
    });
    
    it('should throw an error when the env is null', () => {
      expect(() => { new Logger({ env: null, level: 'debug', logStream: LogStream.FILE }) })
        .to.throw(Errors.ENV_ILLEGAL);
    });
  
    it('should throw an error when the env is undefined', () => {
      expect(() => { new Logger({ env: undefined, level: 'debug', logStream: LogStream.FILE }) })
        .to.throw(Errors.ENV_ILLEGAL);
    });
  
    it('should throw an error when the env has illegal type', () => {
      expect(() => { new Logger({ env: '{"1": "2"}', level: 'debug', logStream: LogStream.FILE }) })
        .to.throw(Errors.ENV_ILLEGAL);
    });
  
    
    it('should throw an error when the env is empty', () => {
      expect(() => { new Logger({ env: {}, level: 'debug', logStream: LogStream.FILE }) })
      .to.throw(Errors.ENV_ILLEGAL);
    });

  });

  describe('level argument', () => {
    it('should throw an error when the level is missing', () => {
      expect(() => { new Logger({ env: config, logStream: LogStream.FILE }) })
        .to.throw(Errors.LEVEL_ILLEGAL);
    });
  
    it('should throw an error when the level is null', () => {
      expect(() => { new Logger({ env: config, level: null, logStream: LogStream.FILE }) })
        .to.throw(Errors.LEVEL_ILLEGAL);
    });
  
    it('should throw an error when the level is undefined', () => {
      expect(() => { new Logger({ env: config, level: undefined, logStream: LogStream.FILE }) })
        .to.throw(Errors.LEVEL_ILLEGAL);
    });
  
    it('should throw an error when the level has illegal value', () => {
      expect(() => { new Logger({ env: config, level: 25, logStream: LogStream.FILE }) })
        .to.throw(Errors.LEVEL_ILLEGAL);
    });
  });

  describe('logStream argument', () => {
    it('should throw an error when the logStream is missing', () => {
      expect(() => { new Logger({ env: config, level: 'debug' }) })
        .to.throw(Errors.STREAM_ILLEGAL);
    });
  
    it('should throw an error when the logStream is null', () => {
      expect(() => { new Logger({ env: config, level: 'debug', logStream: null }) })
        .to.throw(Errors.STREAM_ILLEGAL);
    });
  
    it('should throw an error when the logStream is undefined', () => {
      expect(() => { new Logger({ env: config, level: 'debug', logStream: undefined }) })
        .to.throw(Errors.STREAM_ILLEGAL);
    });
  
    it('should throw an error when the logStream has illegal type', () => {
      expect(() => { new Logger({ env: config, level: 'debug', logStream: 2 }) })
        .to.throw(Errors.STREAM_ILLEGAL);
    });

    it('should throw an error when the logStream has illegal value', () => {
      expect(() => { new Logger({ env: config, level: 'debug', logStream: 'files' }) })
        .to.throw(Errors.STREAM_ILLEGAL);
    });
  });
    
});