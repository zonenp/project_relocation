# Vision
  In-depth learning of Javascript technologies, both for front-end and back-end development.
  Make some money if this thing goes viral, yo!

# Primary milestone
  Create a prototype on local machine that allows the user provide a String search query and get a list of results from different search engines.

# Choice of technologies
  1. Javascript - we love web and this is the number #1 choice for web development.
  2. [Node.js](https://nodejs.org/en/) - backend development framework.
  3. [Angular 2](https://angular.io/) - frontend development framework.
  4. [AWS elasticbeanstalk](https://aws.amazon.com/) - Server.

# Udemy web courses
  [Udemy](http://udemy.com)
  account : danisegal4@gmail.com
  password: ronengay